<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nilai_Kriteria extends Model
{
    //
    protected $table='nilai_kriteria'; //diisi pake nama tabel didatabase
    protected $primaryKey='id_nilai_kriteria'; //primaryKey tabel kriteria nya
    //public $incrementing=false;

    protected $guarded=[
        'id_nilai_kriteria'
    ];

    function kriteria(){
        return $this->belongsTo('App\Model\Kriteria','id_kriteria');
    }
}
