<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    //
    protected $table='buku'; //diisi pake nama tabel
    protected $primaryKey='id_buku'; //primaryKey tabel buku nya
    //public $incrementing=false;

    protected $guarded=[
        'id_buku'
    ];
    
}
