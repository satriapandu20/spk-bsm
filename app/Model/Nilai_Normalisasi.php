<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nilai_Normalisasi extends Model
{
    //
    protected $table='nilai_normalisasi'; //diisi pake nama tabel
    protected $primaryKey='id_nilai_normalisasi'; //primaryKey tabel siswa nya
    //public $incrementing=false;

    protected $guarded=[
        'id_nilai_normalisasi'
    ];

    function siswa(){
        return $this->belongsTo('App\Model\Siswa','id_siswa');
    }
}