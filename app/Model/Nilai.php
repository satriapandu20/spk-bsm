<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    //
    protected $table='nilai'; //diisi pake nama tabel
    protected $primaryKey='id_nilai'; //primaryKey tabel siswa nya
    //public $incrementing=false;

    protected $guarded=[
        'id_nilai'
    ];

    function siswa(){
        return $this->belongsTo('App\Model\Siswa','id_siswa');
    }
}
