<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    //
    protected $table='siswa'; //diisi pake nama tabel
    protected $primaryKey='id_siswa'; //primaryKey tabel siswa nya
    //public $incrementing=false;

    protected $guarded=[
        'id_siswa'
    ];

    function nilai(){
        return $this->hasMany('App\Model\Nilai','id_siswa');
    }

    function nilai_normalisasi(){
        return $this->hasMany('App\Model\Nilai_Normalisasi','id_siswa');
    }

    function nilai_ternormalisasi(){
        return $this->hasMany('App\Model\Nilai_Ternormalisasi','id_siswa');
    }

    function nilai_terno_terbobot(){
        return $this->hasMany('App\Model\Nilai_Terno_Terbobot','id_siswa');
    }

    function nilai_solusi_ideal(){
        return $this->hasMAny('App\Model\Nilai_Solusi_Ideal','id_siswa');
    }

    
    function nilai_jarak_solusi_ideal(){
        return $this->hasMAny('App\Model\Nilai_Solusi_Ideal','id_siswa');
    }
 
}
