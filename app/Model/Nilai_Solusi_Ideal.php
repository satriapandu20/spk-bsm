<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nilai_Solusi_Ideal extends Model
{
    //
    protected $table='nilai_solusi_ideal'; //diisi pake nama tabel
    protected $primaryKey='id_nilai_solusi_ideal'; //primaryKey tabel siswa nya
    //public $incrementing=false;
    
    protected $guarded=[
        'id_nilai_solusi_ideal'
    ];
    
    function siswa(){
        return $this->belongsTo('App\Model\Siswa','id_siswa');
    }
}
