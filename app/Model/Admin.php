<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    //
    protected $table='admin'; //diisi pake nama tabel
    protected $primaryKey='id_admin'; //primaryKey tabel siswa nya
    //public $incrementing=false;

    protected $guarded=[
        'id_admin'
    ];
}
