<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kriteria extends Model
{
    //
    protected $table='kriteria'; //diisi pake nama tabel didatabase
    protected $primaryKey='id_kriteria'; //primaryKey tabel kriteria nya
    //public $incrementing=false;

    protected $guarded=[
        'id_kriteria'
    ];

    function nilai_kriteria(){
        return $this->hasMany('App\Model\Nilai_Kriteria','id_kriteria');
    }

    function nilai(){
        return $this->hasMany('App\Model\Nilai','id_kriteria');
    }
}
