<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nilai_Ternormalisasi extends Model
{
    //
    protected $table='nilai_ternormalisasi'; //diisi pake nama tabel
    protected $primaryKey='id_nilai_ternormalisasi'; //primaryKey tabel siswa nya
    //public $incrementing=false;
    
    protected $guarded=[
        'id_nilai_ternormalisasi'
    ];
    
    function siswa(){
        return $this->belongsTo('App\Model\Siswa','id_siswa');
    }
}
