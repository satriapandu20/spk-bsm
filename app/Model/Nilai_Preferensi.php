<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nilai_Preferensi extends Model
{
    //
    protected $table='nilai_preferensi'; //diisi pake nama tabel
    protected $primaryKey='id_nilai_preferensi'; //primaryKey tabel siswa nya
    //public $incrementing=false;
    
    protected $guarded=[
        'id_nilai_preferensi'
    ];
    
    function siswa(){
        return $this->belongsTo('App\Model\Siswa','id_siswa');
    }
}
