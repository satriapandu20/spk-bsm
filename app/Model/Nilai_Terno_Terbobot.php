<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nilai_Terno_Terbobot extends Model
{
     //
     protected $table='nilai_terno_terbobot'; //diisi pake nama tabel
     protected $primaryKey='id_nilai_terno_terbobot'; //primaryKey tabel siswa nya
     //public $incrementing=false;
     
     protected $guarded=[
         'id_nilai_terno_terbobot'
     ];
     
     function siswa(){
         return $this->belongsTo('App\Model\Siswa','id_siswa');
     }
}
