<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Nilai;
use App\Model\Siswa;
use App\Model\Kriteria;
use App\Model\Nilai_Kriteria;
use App\Model\Nilai_Normalisasi;
use App\Model\Nilai_Ternormalisasi;
use App\Model\Nilai_Terno_Terbobot;
use App\Model\Nilai_Solusi_Ideal;
use App\Model\Nilai_Jarak_Solusi_Ideal;
use App\Model\Nilai_Preferensi;
use PDF;
use Session;

class TOPSISController extends Controller
{

    public function printhasil()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::join('nilai_preferensi','siswa.id_siswa','nilai_preferensi.id_siswa')
        ->orderBy('nilai_preferensi','desc')
        ->get();
        $pdf = PDF::loadView('print/print-hasil', ['siswa'=>$siswa])->setPaper('a4');
        return $pdf->stream('Hasil Nilai Preferensi');

    }
    //nampilin data topsis
    public function index()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        
        return view('TOPSIS/index');
    }

    public function dashboard()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        $siswa=Siswa::count();
        return view('dasbor/index',compact('siswa'));
    }
    public function reset()
    {
        set_time_limit(0);
        $nilai_normalisasi=Nilai_Normalisasi::all();
        foreach($nilai_normalisasi as $n){
            $nilai_normalisasi=Nilai_Normalisasi::where('id_nilai_normalisasi',$n->id_nilai_normalisasi)->first();
            $nilai_normalisasi->delete();
        }

        $nilai_ternormalisasi=Nilai_Ternormalisasi::all();
        foreach($nilai_ternormalisasi as $n){
            $nilai_ternormalisasi=Nilai_Ternormalisasi::where('id_nilai_ternormalisasi',$n->id_nilai_ternormalisasi)->first();
            $nilai_ternormalisasi->delete();
        }

        $nilai_terno_terbobot=Nilai_Terno_Terbobot::all();
        foreach($nilai_terno_terbobot as $n){
            $nilai_terno_terbobot=Nilai_Terno_Terbobot::where('id_nilai_terno_terbobot',$n->id_nilai_terno_terbobot)->first();
            $nilai_terno_terbobot->delete();
        }

        $nilai_solusi_ideal=Nilai_Solusi_Ideal::all();
        foreach($nilai_solusi_ideal as $n){
            $nilai_solusi_ideal=Nilai_Solusi_Ideal::where('id_nilai_solusi_ideal',$n->id_nilai_solusi_ideal)->first();
            $nilai_solusi_ideal->delete();
        }

        $nilai_jarak_solusi_ideal=Nilai_Jarak_Solusi_Ideal::all();
        foreach($nilai_jarak_solusi_ideal as $n){
            $nilai_jarak_solusi_ideal=Nilai_Jarak_Solusi_Ideal::where('id_jarak_solusi_ideal',$n->id_jarak_solusi_ideal)->first();
            $nilai_jarak_solusi_ideal->delete();
        }

        $nilai_preferensi=Nilai_Preferensi::all();
        foreach($nilai_preferensi as $n){
            $nilai_preferensi=Nilai_Preferensi::where('id_nilai_preferensi',$n->id_nilai_preferensi)->first();
            $nilai_preferensi->delete();
        }

        return back()->with('success','Berhasil Mengreset TOPSIS');
    }

    public function TOPSIS()
    {
        set_time_limit(0);
        return $this->normalisasi();
        
    }


///////////////////////////////////////////////////////////////////////
    public function normalisasi()
    {
        set_time_limit(0);
        $nilai=Nilai::all();

        foreach ($nilai as $n) {
            $nilai_normalisasi=Nilai_Normalisasi::where('id_nilai',$n->id_nilai)->first();

            if($nilai_normalisasi==null){
                $nilai_normalisasi = new Nilai_Normalisasi();
                $nilai_normalisasi->id_nilai=$n->id_nilai;
                $nilai_normalisasi->id_kriteria=$n->id_kriteria;
                $nilai_normalisasi->id_siswa=$n->id_siswa;
                $nilai_normalisasi->nilai_normalisasi=$n->nilai*$n->nilai;

                $nilai_normalisasi->save();

            }    

        }
        return $this->ternormalisasi();


    }
    public function ternormalisasi()
    {
        set_time_limit(0);
        $nilai=Nilai::all();
   
       

        foreach ($nilai as $n) {
           
            $jumlah_nilai_normalisasi=Nilai_Normalisasi::where('id_kriteria',$n->id_kriteria)->sum('nilai_normalisasi');
            $akar=sqrt($jumlah_nilai_normalisasi);
     
            $nilai_ternormalisasi=Nilai_Ternormalisasi::where('id_nilai',$n->id_nilai)->first();
         
            if($nilai_ternormalisasi==null){
            $nilai_ternormalisasi = new Nilai_Ternormalisasi();
   
                $nilai_ternormalisasi->id_kriteria=$n->id_kriteria;
                $nilai_ternormalisasi->id_nilai=$n->id_nilai;
                $nilai_ternormalisasi->id_siswa=$n->id_siswa;

                $nilai_ternormalisasi->nilai_ternormalisasi=$n->nilai/$akar;

                $nilai_ternormalisasi->save();
            }

        }

        return $this->terno_terbobot();
    }

    public function terno_terbobot()
    {
        set_time_limit(0);
        $nilai_ternormalisasi=Nilai_Ternormalisasi::all();

        foreach ($nilai_ternormalisasi as $nt) {
      
           $kriteria=Kriteria::where('id_kriteria',$nt->id_kriteria)->first();

        
            $nilai_terno_terbobot=Nilai_Terno_Terbobot::where('id_nilai_ternormalisasi',$nt->id_nilai_ternormalisasi)->first();//target

            if($nilai_terno_terbobot==null){
                $nilai_terno_terbobot = new Nilai_Terno_Terbobot();
   
                $nilai_terno_terbobot->id_kriteria=$nt->id_kriteria;
                $nilai_terno_terbobot->id_nilai_ternormalisasi=$nt->id_nilai_ternormalisasi;
                $nilai_terno_terbobot->id_siswa=$nt->id_siswa;

                $nilai_terno_terbobot->nilai_terno_terbobot=$nt->nilai_ternormalisasi*$kriteria->bobot;

                $nilai_terno_terbobot->save();
            }

        }

        return $this->solusi_ideal();
    }

    public function solusi_ideal()
    {
        set_time_limit(0);
        
        $kriteria=Kriteria::all();

        foreach ($kriteria as $k) {

            $nilai_a_plus='';
            if($k->sifat=='benefit'){
                $nilai_a_plus=Nilai_Terno_Terbobot::where('id_kriteria',$k->id_kriteria)->max('nilai_terno_terbobot');
            }else{
                $nilai_a_plus=Nilai_Terno_Terbobot::where('id_kriteria',$k->id_kriteria)->min('nilai_terno_terbobot');

            }
           

            $nilai_a_minus='';
            if($k->sifat=='benefit'){
                $nilai_a_minus=Nilai_Terno_Terbobot::where('id_kriteria',$k->id_kriteria)->min('nilai_terno_terbobot');
            }else{
                $nilai_a_minus=Nilai_Terno_Terbobot::where('id_kriteria',$k->id_kriteria)->max('nilai_terno_terbobot');
            }
           
    

            $solusi_ideal = Nilai_Solusi_Ideal::where('id_kriteria',$k->id_kriteria)->first();
            
            if($solusi_ideal==null){

                $nilai_solusi_ideal= new Nilai_Solusi_Ideal();
                $nilai_solusi_ideal->id_kriteria=$k->id_kriteria;
                $nilai_solusi_ideal->a_plus=$nilai_a_plus;
                $nilai_solusi_ideal->a_minus=$nilai_a_minus;

                $nilai_solusi_ideal->save();
                            
            }
        }

        return $this->jarak_solusi_ideal();
    }

    public function jarak_solusi_ideal()
    {
        set_time_limit(0);
        $siswa=Siswa::with('nilai_terno_terbobot')->get();
        $nilai_solusi_ideal=Nilai_Solusi_Ideal::all();

        foreach ($siswa as $s) {
            $jumlah_d_plus=0;
            $jumlah_d_minus=0;

            foreach ($s->nilai_terno_terbobot as $ntt) {
                $nilai_solusi_ideal=Nilai_Solusi_Ideal::where('id_kriteria',$ntt->id_kriteria)->first();
                $d_plus=$nilai_solusi_ideal->a_plus-$ntt->nilai_terno_terbobot;
                $d_plus_pangkat=$d_plus*$d_plus;
                $jumlah_d_plus=$jumlah_d_plus+$d_plus_pangkat;

                $nilai_solusi_ideal=Nilai_Solusi_Ideal::where('id_kriteria',$ntt->id_kriteria)->first();
                $d_minus=$ntt->nilai_terno_terbobot-$nilai_solusi_ideal->a_minus;
                $d_minus_pangkat=$d_minus*$d_minus;
                $jumlah_d_minus=$jumlah_d_minus+$d_minus_pangkat;
            }

            $akar_jumlah_d_plus=sqrt($jumlah_d_plus);
            $akar_jumlah_d_minus=sqrt($jumlah_d_minus);
            
            $njsi=Nilai_Jarak_Solusi_Ideal::where('id_siswa',$s->id_siswa)->first();

            if($njsi==null){
                $nilai_jarak_solusi_ideal= new Nilai_Jarak_Solusi_Ideal();

                $nilai_jarak_solusi_ideal->id_siswa=$s->id_siswa;
                $nilai_jarak_solusi_ideal->d_plus=$akar_jumlah_d_plus;
                $nilai_jarak_solusi_ideal->d_minus=$akar_jumlah_d_minus;
    
                $nilai_jarak_solusi_ideal->save();

            }

        }
        return $this->preferensi();
    }

    public function preferensi(){
        set_time_limit(0); 
        $siswa=Siswa::all();
        foreach ($siswa as $s){
            $hasil_nilai_preferensi=0;

            $nilai_jarak_solusi_ideal=Nilai_Jarak_Solusi_Ideal::where('id_siswa',$s->id_siswa)->first();
           
            $d_minus_tambah_d_plus=$nilai_jarak_solusi_ideal->d_minus+$nilai_jarak_solusi_ideal->d_plus;
         
            if($d_minus_tambah_d_plus!=0){
                $hasil_nilai_preferensi=$nilai_jarak_solusi_ideal->d_minus/$d_minus_tambah_d_plus;

            }
           
            $np=Nilai_Preferensi::where('id_siswa',$s->id_siswa)->first();

            if($np==null){
                $nilai_preferensi=new Nilai_Preferensi();
                $nilai_preferensi->id_siswa=$s->id_siswa;
                $nilai_preferensi->nilai_preferensi=$hasil_nilai_preferensi;
    
                $nilai_preferensi->save();
            }


        }

        return redirect('data-nilai-preferensi')->with('success','Berhasil Memproses TOPSIS');
    }


}
