<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NilaiKriteriaFormRequest;
use App\Model\Nilai_Kriteria; //wajib 
use App\Model\Kriteria;
use Session;

class Nilai_KriteriaController extends Controller
{
    //data nilai_kriteria
    public function index($id_kriteria)
    {

        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        //all semua tanpa seleksi apapun(mentah)
        //get dengan seleksi
        //first itu data satu tok (kaya edit)
        $nilai_kriteria=Nilai_Kriteria::join('kriteria','nilai_kriteria.id_kriteria','kriteria.id_kriteria')
        ->where('nilai_kriteria.id_kriteria',$id_kriteria)
        ->get();
        
        $kriteria=Kriteria::where('id_kriteria',$id_kriteria)->first();

        return view('nilai_kriteria/index',compact('nilai_kriteria','kriteria'));
    }

    //form nilai_kriteria
    public function form_nilai_kriteria($id_kriteria)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $kriteria=Kriteria::where('id_kriteria',$id_kriteria)->first();

        return view('nilai_kriteria/form-nilai-kriteria',compact('kriteria'));
    }

    //tambah nilai_kriteria
    public function tambah_nilai_kriteria(NilaiKriteriaFormRequest $req, $id_kriteria)
    {

        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $nilai_kriteria=new Nilai_Kriteria();

        $nilai_kriteria->id_kriteria=$id_kriteria;
        $nilai_kriteria->kondisi=$req->kondisi;
        $nilai_kriteria->nilai=$req->nilai;


        $nilai_kriteria->save();

        return redirect('data-nilai-kriteria/'.$id_kriteria)->with('success','Berhasil Menambah Data');
    }

    //form edit data nilai kriteria 
    public function formedit($id_kriteria, $id_nilai_kriteria)
    {   

        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $nilai_kriteria=Nilai_Kriteria::where('id_nilai_kriteria',$id_nilai_kriteria)
        ->where('id_kriteria',$id_kriteria)
        ->first();
        $kriteria=Kriteria::where('id_kriteria',$id_kriteria)
        ->first();
       
        return view('nilai_kriteria/edit-nilai-kriteria',compact('kriteria','nilai_kriteria'));
    }


    //fungsi edit nilai kriteria
    public function edit_nilai_kriteria(NilaiKriteriaFormRequest $req, $id_kriteria, $id_nilai_kriteria)
    {
        
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $nilai_kriteria=Nilai_Kriteria::where('id_nilai_kriteria',$id_nilai_kriteria)
        ->where('id_kriteria',$id_kriteria)
        ->first();
        

   
        $nilai_kriteria->kondisi=$req->kondisi;
        $nilai_kriteria->nilai=$req->nilai;


        $nilai_kriteria->save();

        return redirect('data-nilai-kriteria/'.$id_kriteria)->with('success','Berhasil Mengubah Data');
    }

    //fungsi hapus nilai kriteria
    public function hapus_nilai_kriteria($id_kriteria, $id_nilai_kriteria)
    {
        $nilai_kriteria=Nilai_Kriteria::where('id_nilai_kriteria',$id_nilai_kriteria)
        ->where('id_kriteria',$id_kriteria)
        ->first();
        $nilai_kriteria->delete();

        return back()->with('success','Berhasil menghapus data');

    }
    

}
