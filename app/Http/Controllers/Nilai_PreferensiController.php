<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Nilai_Preferensi;
use App\Model\Nilai_Jarak_Solusi_Ideal;
use App\Model\Siswa;
use Session;

class Nilai_PreferensiController extends Controller
{
    //data nilai preferensi
    public function index()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::join('nilai_preferensi','siswa.id_siswa','nilai_preferensi.id_siswa')
        ->orderBy('nilai_preferensi','desc')
        ->get();

    
        return view('nilai_preferensi/index',compact('siswa'));
    }

    //cari preferensi
    public function cari_nilai_preferensi(Request $req)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::join('nilai_preferensi','siswa.id_siswa','nilai_preferensi.id_siswa')
        ->where('nama','like','%'.$req->keyword.'%')
        ->get();

        return view('nilai_preferensi/index',compact('siswa'));
    }


    public function preferensi(){

        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        
        $siswa=Siswa::all();
        foreach ($siswa as $s){
            $hasil_nilai_preferensi=0;

            $nilai_jarak_solusi_ideal=Nilai_Jarak_Solusi_Ideal::where('id_siswa',$s->id_siswa)->first();
           
            $d_minus_tambah_d_plus=$nilai_jarak_solusi_ideal->d_minus+$nilai_jarak_solusi_ideal->d_plus;
         
            if($d_minus_tambah_d_plus!=0){
                $hasil_nilai_preferensi=$nilai_jarak_solusi_ideal->d_minus/$d_minus_tambah_d_plus;

            }
           
            $np=Nilai_Preferensi::where('id_siswa',$s->id_siswa)->first();

            if($np==null){
                $nilai_preferensi=new Nilai_Preferensi();
                $nilai_preferensi->id_siswa=$s->id_siswa;
                $nilai_preferensi->nilai_preferensi=$hasil_nilai_preferensi;
    
                $nilai_preferensi->save();
            }


        }

        return back()->with('success','Berhasil Nilai Preferensi');
    }

}
