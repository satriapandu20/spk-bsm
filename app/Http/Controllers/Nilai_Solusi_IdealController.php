<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Nilai_Solusi_Ideal;
use App\Model\Nilai_Terno_Terbobot;
use App\Model\Kriteria; //wajib
use App\Model\Siswa;
use Session;

class Nilai_Solusi_IdealController extends Controller
{
    //data nilai solusi ideal
    public function index()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $kriteria=Kriteria::join('nilai_solusi_ideal','kriteria.id_kriteria','nilai_solusi_ideal.id_kriteria')->get();

        return view('nilai_solusi_ideal/index',compact('kriteria'));
    }

    //cari nilai normalisasi
    public function cari_nilai_solusi_ideal(Request $req)
    {

        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $kriteria=Kriteria::join('nilai_solusi_ideal','kriteria.id_kriteria','nilai_solusi_ideal.id_kriteria')
        ->where('nama_kriteria','like','%'.$req->keyword.'%')
        ->get();
   
        return view('nilai_solusi_ideal/index',compact('kriteria'));
    }

    public function solusi_ideal()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        
        $kriteria=Kriteria::all();

        foreach ($kriteria as $k) {

            $nilai_a_plus='';
            if($k->sifat=='benefit'){
                $nilai_a_plus=Nilai_Terno_Terbobot::where('id_kriteria',$k->id_kriteria)->max('nilai_terno_terbobot');
            }else{
                $nilai_a_plus=Nilai_Terno_Terbobot::where('id_kriteria',$k->id_kriteria)->min('nilai_terno_terbobot');

            }
           

            $nilai_a_minus='';
            if($k->sifat=='benefit'){
                $nilai_a_minus=Nilai_Terno_Terbobot::where('id_kriteria',$k->id_kriteria)->min('nilai_terno_terbobot');
            }else{
                $nilai_a_minus=Nilai_Terno_Terbobot::where('id_kriteria',$k->id_kriteria)->max('nilai_terno_terbobot');
            }
           
    

            $solusi_ideal = Nilai_Solusi_Ideal::where('id_kriteria',$k->id_kriteria)->first();
            
            if($solusi_ideal==null){

                $nilai_solusi_ideal= new Nilai_Solusi_Ideal();
                $nilai_solusi_ideal->id_kriteria=$k->id_kriteria;
                $nilai_solusi_ideal->a_plus=$nilai_a_plus;
                $nilai_solusi_ideal->a_minus=$nilai_a_minus;

                $nilai_solusi_ideal->save();
                            
            }
        }

        return back()->with('success','Berhasil Solusi Ideal');
    }
}
