<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SiswaFormRequest;
use App\Model\Siswa;
use App\Model\Nilai;
use App\Model\Nilai_Normalisasi;
use App\Model\Nilai_Ternormalisasi;
use App\Model\Nilai_Terno_Terbobot;
use App\Model\Nilai_Solusi_Ideal;
use App\Model\Nilai_Jarak_Solusi_Ideal;
use App\Model\Nilai_Preferensi;
use Session;



class SiswaController extends Controller
{
    //data siswa 
    public function index()
    {
          ///pengaman 
          if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
          {
              return redirect('login');
          }
          ////
        $siswa=Siswa::all();

        return view('siswa/index',compact('siswa'));
    }
    //form siswa
    public function formsiswa()
    {
          ///pengaman 
          if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
          {
              return redirect('login');
          }
          ////
        return view('siswa/form-siswa');
    }

    //tambah siswa
    public function tambahsiswa(SiswaFormRequest $req)
    {


        $siswa=new Siswa();

        
        $siswa->NIK=$req->NIK;
        $siswa->kelas=$req->kelas;
        $siswa->rombel=$req->rombel;
        $siswa->tahun_ajaran=$req->tahun_ajaran;
        $siswa->nama=$req->nama;
        $siswa->JK=$req->JK;
        $siswa->tempat_lahir=$req->tempat_lahir;
        $siswa->tanggal_lahir=$req->tanggal_lahir;
        $siswa->alamat=$req->alamat;

        $siswa->save();

        return redirect('data-siswa')->with('success','Berhasil Menambah Data');
    }
    //form edit data siswa 
    public function formedit($id_siswa)
    {   
          ///pengaman 
          if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
          {
              return redirect('login');
          }
          ////
        $siswa=Siswa::where('id_siswa',$id_siswa)->first();
        return view('siswa/edit-siswa',compact('siswa'));

    }
    //fungsi edit siswa
    public function editsiswa(SiswaFormRequest $req, $id_siswa)
    {
          ///pengaman 
          if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
          {
              return redirect('login');
          }
          ////

        $siswa=Siswa::where('id_siswa',$id_siswa)->first();

        
        $siswa->NIK=$req->NIK;
        $siswa->kelas=$req->kelas;
        $siswa->rombel=$req->rombel;
        $siswa->tahun_ajaran=$req->tahun_ajaran;
        $siswa->nama=$req->nama;
        $siswa->JK=$req->JK;
        $siswa->tempat_lahir=$req->tempat_lahir;
        $siswa->tanggal_lahir=$req->tanggal_lahir;
        $siswa->alamat=$req->alamat;

        $siswa->save();

        return redirect('data-siswa')->with('success','Berhasil Mengubah Data');
    }

    //fungsi hapus siswa
    public function hapussiswa($id_siswa)
    {
        $siswa=Siswa::where('id_siswa',$id_siswa)->get();
        foreach ($siswa as $n){
            $siswa=Siswa::where('id_siswa',$n->id_siswa)->first();
            $siswa->delete();
        }
        


        $nilai=Nilai::where('id_siswa',$id_siswa)->get();
        foreach($nilai as $n){
            $nilai=Nilai::where('id_nilai',$n->id_nilai)->first();
            $nilai->delete();
        }

        $nilai_normalisasi=Nilai_Normalisasi::where('id_siswa',$id_siswa)->get();
        foreach($nilai_normalisasi as $n){
            $nilai_normalisasi=Nilai_Normalisasi::where('id_nilai_normalisasi',$n->id_nilai_normalisasi)->first();
            $nilai_normalisasi->delete();
        }

        $nilai_ternormalisasi=Nilai_Ternormalisasi::where('id_siswa',$id_siswa)->get();
        foreach($nilai_ternormalisasi as $n){
            $nilai_ternormalisasi=Nilai_Ternormalisasi::where('id_nilai_ternormalisasi',$n->id_nilai_ternormalisasi)->first();
            $nilai_ternormalisasi->delete();
        }

        $nilai_terno_terbobot=Nilai_Terno_Terbobot::where('id_siswa',$id_siswa)->get();
        foreach($nilai_terno_terbobot as $n){
            $nilai_terno_terbobot=Nilai_Terno_Terbobot::where('id_nilai_terno_terbobot',$n->id_nilai_terno_terbobot)->first();
            $nilai_terno_terbobot->delete();
        }

        // $nilai_solusi_ideal=Nilai_Solusi_Ideal::where('id_siswa',$id_siswa)->get();
        // foreach($nilai_solusi_ideal as $n){
        //     $nilai_solusi_ideal=Nilai_Solusi_Ideal::where('id_nilai_solusi_ideal',$n->id_nilai_solusi_ideal)->first();
        //     $nilai_solusi_ideal->delete();
        // }

        $nilai_jarak_solusi_ideal=Nilai_Jarak_Solusi_Ideal::where('id_siswa',$id_siswa)->get();
        foreach($nilai_jarak_solusi_ideal as $n){
            $nilai_jarak_solusi_ideal=Nilai_Jarak_Solusi_Ideal::where('id_jarak_solusi_ideal',$n->id_jarak_solusi_ideal)->first();
            $nilai_jarak_solusi_ideal->delete();
        }

        $nilai_preferensi=Nilai_Preferensi::where('id_siswa',$id_siswa)->get();
        foreach($nilai_preferensi as $n){
            $nilai_preferensi=Nilai_Preferensi::where('id_nilai_preferensi',$n->id_nilai_preferensi)->first();
            $nilai_preferensi->delete();
        }

        
    
       

        return redirect('data-siswa')->with('success','Berhasil menghapus data');

    }


}
