<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\NilaiFormRequest;
use App\Model\Nilai; //wajib
use App\Model\Siswa; //wajib
use App\Model\Kriteria; //wajib
use App\Model\Nilai_Kriteria; //wajib
use Session;

class NilaiController extends Controller
{
    //data nilai
    public function index()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::with('nilai')->get();
        
        
        $kriteria=Kriteria::all();

        return view('nilai/index',compact('siswa','kriteria'));
    }

    //cari siswa
    public function cari_siswa(Request $req)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::where('NIK','like','%'.$req->keyword.'%')
        ->orwhere('nama','like','%'.$req->keyword.'%')
        ->with('nilai')
        ->get();
        $kriteria=Kriteria::all();
        return view('nilai/index',compact('siswa','kriteria'));
    }

    //form nilai
    public function form_nilai($id_siswa)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $kriteria=Kriteria::with('nilai_kriteria')->get();
        // $nilai_kriteria=Nilai_Kriteria::join('kriteria','nilai_kriteria.id_kriteria','kriteria.id_kriteria')->get();
        $siswa=Siswa::where('id_siswa',$id_siswa)->first();
        
        $nilai=Nilai::join('kriteria','nilai.id_kriteria','kriteria.id_kriteria')
                    ->where('id_siswa',$id_siswa)
                    ->get();

        return view('nilai/form-nilai',compact('kriteria','siswa','nilai'));
    }

    //form tambah nilai
    public function tambah_nilai(NilaiFormRequest $req, $id_siswa)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
       
        foreach($req->nilai_kriteria as $key => $nilai_kriteria)
        {
            $nilai=new Nilai();

          
            if(strlen($key)=='3'){
                $key=substr($key,1,1);
            }

            elseif(strlen($key)=='4'){
                $key=substr($key,1,2);

            }

            
            $nilai->id_kriteria=$key;
            $nilai->id_siswa=$id_siswa;
            $nilai->nilai=$nilai_kriteria;
            
            $nilai->save();
            
        }



        return redirect('data-nilai')->with('success','Berhasil Menambah Data');
    }
    
     //fungis edit nilai
     public function edit_nilai(Request $req, $id_siswa)
     {
         ///pengaman 
         if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
         {
             return redirect('login');
         }
         ////
         
        foreach($req->nilai_kriteria as $key => $nilai_kriteria)
        {
         
            if(strlen($key)=='3'){
                $key=substr($key,1,1);
            }

            elseif(strlen($key)=='4'){
                $key=substr($key,1,2);

            }
         $nilai=Nilai::where('id_siswa',$id_siswa)
         ->where('id_kriteria',$key)
         ->first();
         
        
         $nilai->nilai=$nilai_kriteria;
            
         $nilai->save();

        }

        return redirect('data-nilai')->with('success','Berhasil Mengedit Data');

     }

     public function reset()
     {
        $nilai=Nilai::all();
        foreach($nilai as $n){
            $nilai=Nilai::all();
            $n->delete();
        }

        return back()->with('success','Berhasil Mengreset Nilai');

     }


}
