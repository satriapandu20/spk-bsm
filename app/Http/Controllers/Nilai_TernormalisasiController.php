<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Nilai_Ternormalisasi; //wajib
use App\Model\Nilai_Normalisasi;
use App\Model\Nilai;
use App\Model\Siswa; //wajib
use App\Model\Kriteria; //wajib
use App\Model\Nilai_Kriteria; //wajib
use Session;


class Nilai_TernormalisasiController extends Controller
{

    //data nilai ternormalisasi
    public function index()
    {

        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::with('nilai_ternormalisasi')->get();

        $kriteria=Kriteria::all();

        return view('nilai_ternormalisasi/index',compact('siswa','kriteria'));
    }

    //cari nilai ternormalisasi
    public function cari_nilai_ternormalisasi(Request $req)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::where('NIK','like','%'.$req->keyword.'%')
        ->orwhere('nama','like','%'.$req->keyword.'%')
        ->with('nilai_ternormalisasi')
        ->get();
        $kriteria=Kriteria::all();
        return view('nilai_ternormalisasi/index',compact('siswa','kriteria'));
    }    

    public function ternormalisasi()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        
        $nilai=Nilai::all();
   
       

        foreach ($nilai as $n) {
           
            $jumlah_nilai_normalisasi=Nilai_Normalisasi::where('id_kriteria',$n->id_kriteria)->get();
            
            dd($jumlah_nilai_normalisasi);
            $nilai_ternormalisasi=Nilai_Ternormalisasi::where('id_nilai',$n->id_nilai)->first();
         
            if($nilai_ternormalisasi==null){
            $nilai_ternormalisasi = new Nilai_Ternormalisasi();
   
                $nilai_ternormalisasi->id_kriteria=$n->id_kriteria;
                $nilai_ternormalisasi->id_nilai=$n->id_nilai;
                $nilai_ternormalisasi->id_siswa=$n->id_siswa;

                $nilai_ternormalisasi->nilai_ternormalisasi=$n->nilai/$akar;

                $nilai_ternormalisasi->save();
            }

        }

        return back()->with('success','Berhasil Ternormalisasi');
    }
}
