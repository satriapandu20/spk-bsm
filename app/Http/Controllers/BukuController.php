<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Buku; //wajib

class BukuController extends Controller
{
    //data buku
    public function index()
    {
        //views=model
        $buku=Buku::all();

        return view('buku/index',compact('buku'));
    }
    //form buku
    public function formbuku()
    {
        //folder/file folder
        return view('buku/form-buku');
    }

    //tambah buku
    public function tambahbuku(Request $req)
    {
        $req->validate([
            'judul_buku'=>'required',
            'penerbit'=>'required',
            'tahun_terbit'=>'required',
            'tanggal_terbit'=>'required',
            'ketersediaan'=>'required',
            'keterangan'=>'required'

        ]);

        $buku=new Buku();

        $buku->judul_buku=$req->judul_buku;
        $buku->penerbit=$req->penerbit;
        $buku->tahun_terbit=$req->tahun_terbit;
        $buku->tanggal_terbit=$req->tanggal_terbit;
        $buku->ketersediaan=$req->ketersediaan;
        $buku->keterangan=$req->keterangan;

        $buku->save();

        return redirect('data-buku')->with('success','Berhasil Menambah Data');
    }
    //form edit data buku 
    public function formedit($id_buku)
    {   
        $buku=Buku::where('id_buku',$id_buku)->first();
        return view('buku/edit-buku',compact('buku'));
    }
    //fungsi edit buku
    public function editbuku(Request $req, $id_buku)
    {
        
        $buku=Buku::where('id_buku',$id_buku)->first();


        $buku->judul_buku=$req->judul_buku;
        $buku->penerbit=$req->penerbit;
        $buku->tahun_terbit=$req->tahun_terbit;
        $buku->tanggal_terbit=$req->tanggal_terbit;
        $buku->ketersediaan=$req->ketersediaan;
        $buku->keterangan=$req->keterangan;

        $buku->save();

        return redirect('edit-buku/'.$id_buku)->with('success','Berhasil Mengubah Data');
    }
    //fungsi hapus buku
    public function hapusbuku($id_buku)
    {
        $buku=Buku::where('id_buku',$id_buku)->first();
        $buku->delete();

        return redirect('data-buku')->with('success','Berhasil menghapus data');

    }            
}
