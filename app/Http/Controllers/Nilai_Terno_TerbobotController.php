<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Nilai_Terno_Terbobot;
use App\Model\Nilai_Ternormalisasi; //wajib
use App\Model\Nilai;
use App\Model\Siswa; 
use App\Model\Kriteria; //wajib
use Session;


class Nilai_Terno_TerbobotController extends Controller
{
    //data nilai ternormalisasi
    public function index()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::with('nilai_terno_terbobot')->get();

        $kriteria=Kriteria::all();

        return view('nilai_terno_terbobot/index',compact('siswa','kriteria'));
    }

    //cari nilai terno_terbobot
    public function cari_nilai_terno_terbobot(Request $req)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::where('NIK','like','%'.$req->keyword.'%')
        ->orwhere('nama','like','%'.$req->keyword.'%')
        ->with('nilai_terno_terbobot')
        ->get();
        $kriteria=Kriteria::all();
        return view('nilai_terno_terbobot/index',compact('siswa','kriteria'));
    }   

    public function terno_terbobot()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        
        $nilai_ternormalisasi=Nilai_Ternormalisasi::all();

        foreach ($nilai_ternormalisasi as $nt) {
      
           $kriteria=Kriteria::where('id_kriteria',$nt->id_kriteria)->first();

        
            $nilai_terno_terbobot=Nilai_Terno_Terbobot::where('id_nilai_ternormalisasi',$nt->id_nilai_ternormalisasi)->first();//target

            if($nilai_terno_terbobot==null){
                $nilai_terno_terbobot = new Nilai_Terno_Terbobot();
   
                $nilai_terno_terbobot->id_kriteria=$nt->id_kriteria;
                $nilai_terno_terbobot->id_nilai_ternormalisasi=$nt->id_nilai_ternormalisasi;
                $nilai_terno_terbobot->id_siswa=$nt->id_siswa;

                $nilai_terno_terbobot->nilai_terno_terbobot=$nt->nilai_ternormalisasi*$kriteria->bobot;

                $nilai_terno_terbobot->save();
            }

        }

        return back()->with('success','Berhasil Ternormalisasi Terbobot');
    }
}
