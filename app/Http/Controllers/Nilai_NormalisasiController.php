<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Nilai_Normalisasi; //wajib
use App\Model\Nilai;
use App\Model\Siswa; //wajib
use App\Model\Kriteria; //wajib
use App\Model\Nilai_Kriteria; //wajib
use Session;


class Nilai_NormalisasiController extends Controller
{
    //data nilai normalisasi
    public function index()
    {

        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::with('nilai_normalisasi')->get();
        
        
        $kriteria=Kriteria::all();

        return view('nilai_normalisasi/index',compact('siswa','kriteria'));
    }
    //cari nilai normalisasi
    public function cari_nilai_normalisasi(Request $req)
    {

        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::where('NIK','like','%'.$req->keyword.'%')
        ->orwhere('nama','like','%'.$req->keyword.'%')
        ->with('nilai_normalisasi')
        ->get();
        $kriteria=Kriteria::all();
        return view('nilai_normalisasi/index',compact('siswa','kriteria'));
    }

    public function normalisasi()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        
        $nilai=Nilai::all();

        foreach ($nilai as $n) {
            $nilai_normalisasi=Nilai_Normalisasi::where('id_nilai',$n->id_nilai)->first();

            if($nilai_normalisasi==null){
                $nilai_normalisasi = new Nilai_Normalisasi();
                $nilai_normalisasi->id_nilai=$n->id_nilai;
                $nilai_normalisasi->id_kriteria=$n->id_kriteria;
                $nilai_normalisasi->id_siswa=$n->id_siswa;
                $nilai_normalisasi->nilai_normalisasi=$n->nilai*$n->nilai;
    
                $nilai_normalisasi->save();

            }

         

        }

        return back()->with('success','Berhasil Normalisasi');

    }
}