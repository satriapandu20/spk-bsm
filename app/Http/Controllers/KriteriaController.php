<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KriteriaFormRequest;
use App\Model\Kriteria; //wajib
use App\Model\Nilai_Kriteria;
use App\Model\Nilai;
use Session;

class KriteriaController extends Controller
{
    //data kriteria 
    public function index()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $kriteria=Kriteria::all();

        return view('kriteria/index',compact('kriteria'));
    }
    //form kriteria
    public function formkriteria()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        
        return view('kriteria/form-kriteria');
    }

    //tambah kriteria
    public function tambahkriteria(KriteriaFormRequest $req)
    {

        $kriteria=new Kriteria();

        $kriteria->nama_kriteria=$req->nama_kriteria;
        $kriteria->bobot=$req->bobot;
        $kriteria->sifat=$req->sifat;
        $kriteria->keterangan=$req->keterangan;

        $kriteria->save();

        return redirect('data-kriteria')->with('success','Berhasil Menambah Data');
    }
    //form edit data kriteria 
    public function formedit($id_kriteria)
    {   
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        
        $kriteria=Kriteria::where('id_kriteria',$id_kriteria)->first();
        return view('kriteria/edit-kriteria',compact('kriteria'));
    }
    //fungsi edit kriteria
    public function editkriteria(KriteriaFormRequest $req, $id_kriteria)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $kriteria=Kriteria::where('id_kriteria',$id_kriteria)->first();


        $kriteria->nama_kriteria=$req->nama_kriteria;
        $kriteria->bobot=$req->bobot;
        $kriteria->sifat=$req->sifat;
        $kriteria->keterangan=$req->keterangan;

        $kriteria->save();

        return redirect('data-kriteria')->with('success','Berhasil Mengubah Data');
    }

    //fungsi hapus kriteria
    public function hapuskriteria($id_kriteria)
    {
        $kriteria=Kriteria::where('id_kriteria',$id_kriteria)->first();
        $kriteria->delete();

        $nilai=Nilai::where('id_kriteria',$id_kriteria)->get();
        foreach($nilai as $n){
            $nilai=Nilai::where('id_nilai',$n->id_nilai)->first();
            $nilai->delete();
        }

        $nilai_kriteria=Nilai_Kriteria::where('id_kriteria',$id_kriteria)->get();
        foreach($nilai_kriteria as $n){
            $nilai_kriteria=Nilai_Kriteria::where('id_nilai_kriteria',$n->id_nilai_kriteria)->first();
            $nilai_kriteria->delete();
        }

        return redirect('data-kriteria')->with('success','Berhasil menghapus data');

    }

}
