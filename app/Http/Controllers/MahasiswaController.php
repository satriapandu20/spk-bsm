<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    //form buku
    public function formmahasiswa()
    {
        //folder/file folder
        return view('mahasiswa/form-mahasiswa');
    }

    //tambah buku
    public function tambahmahasiswa(Request $req)
    {

        //jika nama_depan tidak kosong, jika nama_depan=alfika maka nilai=10, jika nama_depan=pandu maka nilai=5, 
        //jika nama_depan=ade maka nilai=1, jika nama_depan kosong maka error
        //jika nilai=1 maka predikat=tidak cukup, jika nilai=5 maka predikat= cukup, jika nilai=10 maka predikat= sangat cukup,
        //jika predikat=sangat cukup maka kelulusan=lulus
        $nama_depan=$req->nama_depan;
        //$nilai=$req->nilai;
        $nilai='';
        $predikat='';   
        $kelulusan='';

        if($nama_depan!=''){

            if($nama_depan=='alfika'){
                $nilai='10';
                
            }


            elseif($nama_depan=='pandu'){
                $nilai='5';
            }
            elseif($nama_depan=='ade'){
                $nilai='1';
            }
                if($nilai=='1'){
                    $predikat='tidak cukup';
                }
                elseif($nilai=='5'){
                    $predikat='cukup';
                }
                elseif($nilai=='10'){
                    $predikat='sangat cukup';
                }
                    if($predikat=='tidak cukup'||$predikat=='cukup'){
                        $kelulusan='TIDAK LULUS';
                    }
                    elseif($predikat=='sangat cukup'){
                        $kelulusan='LULUS';
                    }
           

        } 
        elseif($nama_depan==''){
            $nilai='ERROR';
        }

        
            

        //return back()->with('success','nama :'.$nama);
        return back()->with('success','nama_depan :'.$nama_depan. 'nilai :'.$nilai. 'predikat :'.$predikat.$kelulusan);
    }

}
