<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\Nilai_Jarak_Solusi_Ideal;
use App\Model\Nilai_Solusi_Ideal;
use App\Model\Nilai_Terno_Terbobot;
use App\Model\Siswa; //wajib
use App\Model\Kriteria; //wajib
use Session;

class Nilai_Jarak_Solusi_IdealController extends Controller
{
    //data nilai jarak solusi ideal
    public function index()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::join('nilai_jarak_solusi_ideal','siswa.id_siswa','nilai_jarak_solusi_ideal.id_siswa')->get();
        $kriteria=Kriteria::all();

        return view('nilai_jarak_solusi_ideal/index',compact('siswa','kriteria'));
    }

    //cari siswa
    public function cari_nilai_jarak_solusi_ideal(Request $req)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////

        $siswa=Siswa::join('nilai_jarak_solusi_ideal','siswa.id_siswa','nilai_jarak_solusi_ideal.id_siswa')
        ->where('nama','like','%'.$req->keyword.'%')
        ->get();

        return view('nilai_jarak_solusi_ideal/index',compact('siswa'));
    }

        // //cari nilai normalisasi
        // public function cari_nilai_solusi_ideal(Request $req)
        // {
        //     $kriteria=Kriteria::join('nilai_solusi_ideal','kriteria.id_kriteria','nilai_solusi_ideal.id_kriteria')
        //     ->where('nama_kriteria','like','%'.$req->keyword.'%')
        //     ->get();
       
        //     return view('nilai_solusi_ideal/index',compact('kriteria'));
        // }


    
    public function jarak_solusi_ideal()
    {
        $siswa=Siswa::with('nilai_terno_terbobot')->get();
        $nilai_solusi_ideal=Nilai_Solusi_Ideal::all();

        foreach ($siswa as $s) {
            $jumlah_d_plus=0;
            $jumlah_d_minus=0;

            foreach ($s->nilai_terno_terbobot as $ntt) {
                $nilai_solusi_ideal=Nilai_Solusi_Ideal::where('id_kriteria',$ntt->id_kriteria)->first();
                $d_plus=$nilai_solusi_ideal->a_plus-$ntt->nilai_terno_terbobot;
                $d_plus_pangkat=$d_plus*$d_plus;
                $jumlah_d_plus=$jumlah_d_plus+$d_plus_pangkat;

                $nilai_solusi_ideal=Nilai_Solusi_Ideal::where('id_kriteria',$ntt->id_kriteria)->first();
                $d_minus=$ntt->nilai_terno_terbobot-$nilai_solusi_ideal->a_minus;
                $d_minus_pangkat=$d_minus*$d_minus;
                $jumlah_d_minus=$jumlah_d_minus+$d_minus_pangkat;
            }

            $akar_jumlah_d_plus=sqrt($jumlah_d_plus);
            $akar_jumlah_d_minus=sqrt($jumlah_d_minus);
            
            $njsi=Nilai_Jarak_Solusi_Ideal::where('id_siswa',$s->id_siswa)->first();

            if($njsi==null){
                $nilai_jarak_solusi_ideal= new Nilai_Jarak_Solusi_Ideal();

                $nilai_jarak_solusi_ideal->id_siswa=$s->id_siswa;
                $nilai_jarak_solusi_ideal->d_plus=$akar_jumlah_d_plus;
                $nilai_jarak_solusi_ideal->d_minus=$akar_jumlah_d_minus;
    
                $nilai_jarak_solusi_ideal->save();

            }

        }
        return back()->with('success','Berhasil Jarak Solusi');

        
    }
}
