<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AdminFormRequest;
use Illuminate\Http\Request;
use App\Model\Admin;
use App\Model\Kriteria;
use Session;

class AdminController extends Controller
{
    //data admin
    public function index()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        $admin=Admin::all();

        return view('admin/index',compact('admin'));

    }

    //fungsi form admin
    public function formadmin()
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        return view('admin/form-admin');
    }

    //tambah admin
    public function tambahadmin(AdminFormRequest $req)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////


        $admin=new Admin();

        $admin->username=$req->username;
        $admin->password=bcrypt($req->password);
       

        $admin->save();

        return redirect('data-admin')->with('success','Berhasil Menambah Data');
    }

    //form edit data kriteria 
    public function formedit($id_admin)
    {   
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        $admin=Admin::where('id_admin',$id_admin)->first();
        return view('admin/edit-admin',compact('admin'));
    }
    //fungsi edit kriteria
    public function editadmin(AdminFormRequest $req, $id_admin)
    {
        ///pengaman 
        if(!(Session::get('login'))&&(!(Session::get('admin'))||!(Session::get('superadmin'))))
        {
            return redirect('login');
        }
        ////
        
        $admin=Admin::where('id_admin',$id_admin)->first();


        $admin->username=$req->username;
        $admin->password=bcrypt($req->password);
       
        $admin->save();

        return redirect('data-admin')->with('success','Berhasil Mengubah Data');
    }

    //fungsi hapus kriteria
    public function hapusadmin($id_admin)
    {
        $admin=Admin::where('id_admin',$id_admin)->first();
        $admin->delete();


        return redirect('data-admin')->with('success','Berhasil menghapus data');
    }

    public function proseslogin(Request $req)
    {
        $username=$req->username;
        $password=$req->password;

        $admin=Admin::where('username',$username)->first();
        
        if(Hash::check($password,$admin->password)){
            if($admin->status=='admin')
            {
                Session::put('admin',$admin->id_admin);
                Session::put('login',TRUE);


                return redirect('');
            }
            else{
                Session::put('superadmin',$admin->id_admin);
                Session::put('login',TRUE);
                
                return redirect('/');
            }

           
        }else{
            return back()->with('error','username atau password salah');
        }
        
    }
    public function proseslogout()
    {
        Session::flush();

        return redirect('login');
    }
    
}