<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NilaiKriteriaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kondisi'=>'required',
            'nilai'=>'required'
            //
        ];
    }
    public function messages()
    {
        return [
            'kondisi.required'=>'Kondisi Harus diisi',
            'nilai.required'=>'Nilai Harus dipilih',

            

        ];

       
    }
}
