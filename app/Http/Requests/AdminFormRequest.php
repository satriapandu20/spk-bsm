<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'username'=>'required',
            'password'=>'required|confirmed|min:6',
            'password_confirmation'=>'required|min:6'
        ];
    }
    public function messages()
    {
        return [
            'username.required'=>'Username Harus diisi',
            'password.required'=>'Password Harus diisi',
            'password.confirmed'=>'Password Tidak Cocok',
            'password.min'=>'Password Harus 6 Digit',
            'password_confirmation.required'=>'Konfirmasi Password Harus diisi',
            'password_confirmation.min'=>'Password Harus 6 Digit',
        ];
    }
}
