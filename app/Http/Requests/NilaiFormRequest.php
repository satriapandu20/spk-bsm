<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Model\Kriteria;
class NilaiFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        
        foreach ($this->request->get('nilai_kriteria') as $key => $val)
         {
            if(strlen($key)=='3'){
                $key=substr($key,1,1);
            }

            elseif(strlen($key)=='4'){
                $key=substr($key,1,2);

            }
            $kriteria=Kriteria::where('id_kriteria',$key)->first();
            
            
           $rules['nilai_kriteria.'.$kriteria->nama_kriteria] = 'required';
        }
        return $rules;
    }
    public function messages()
    {
        $messages = [];
        foreach ($this->request->get('nilai_kriteria') as $key => $val) 
        {
            $messages['nilai_kriteria.'.$key]= 'Lengkapi Data Kriteria '.$key;
        }
        
        return $messages;
    }
}
