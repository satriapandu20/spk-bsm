<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiswaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'NIK'=>'required|max:16',
            'kelas'=>'required',
            'rombel'=>'required',
            'tahun_ajaran'=>'required',
            'nama'=>'required',
            'JK'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'alamat'=>'required'
            //
        ];
    }
    public function messages()
    {
        return[
            'NIK.required'=>'NIK Harus diisi 16 Digit',
            'kelas.required'=>'Kelas Harus dipilih', 
            'rombel.required'=>'Rombel Harus dipilih',
            'tahun_ajaran.required'=>'Tahun Ajaran Harus dipilih',
            'nama.required'=>'Nama Harus diisi',
            'JK.required'=>'Jenis Kelamin Harus dipilih',
            'tempat_lahir.required'=>'Tempat Lahir Harus diisi',
            'tanggal_lahir.required'=>'Tanggal Lahir Harus diisi atau dipilih',
            'alamat.required'=>'Alamat Harus diisi'
        ];
    }
}
