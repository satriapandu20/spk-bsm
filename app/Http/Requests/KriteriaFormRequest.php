<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KriteriaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'nama_kriteria'=>'required',
            'bobot'=>'required',
            'sifat'=>'required',
            'keterangan'=>'required'
            //
        ];
    }
    public function messages()
    {
        return [
            'nama_kriteria.required'=>'Kriteria Harus diisi',
            'bobot.required'=>'Bobot Harus dipilih',
            'sifat.required'=>'Sifat Harus dipilih',
            'keterangan.required'=>'Keterangan Harus diisi',
        ];
    }
}
