@extends('layout/layout-spk')

@section('title','Data Nilai Normalisasi')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Data Nilai Normalisasi</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> Data Nilai Normalisasi</li>
            </ol>
          </div>
          <form role="form" action="{{ url('cari-nilai-normalisasi') }}" method="get" >
                <div class="form-group">
                    <input class="form-control" type="text" name="keyword" placeholder='Cari nilai siswa...'>
                </div>

                <button type="submit" class="btn btn-default">Cari</button>
          </form>
</div><!-- /.row -->
<br>
<div class="">
          <div class="">
            <div class="">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <!-- <tr>
                    <th class='col-lg-1'>No</th>
                    <th>NIK <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Kelas <i class="fa fa-sort"></i></th>
                    <th  class='col-lg-1'>Tahun Ajaran <i class="fa fa-sort"></i></th>
                    <th>Nama <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Jenis Kelamin <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Tanggal Lahir <i class="fa fa-sort"></i></th>
                    <th class='col-lg-2'> Aksi </th>
                  </tr> -->
                  <?php

                    $jumlah_kriteria=count($kriteria);
                    $nilai_kriteria=array();
                    $id_kriteria=array();
                    
                  ?>

                   
                    <th>No</th>
                    <th>Nama Siswa</th>

                    @foreach($kriteria as $k)
                    <th>{{$k->nama_kriteria}} 
                      @if($k->sifat=='benefit')
                      (B)
                      @else
                      (C)
                      @endif
                    </th>
                    <?php
                      $nilai_kriteria[$k->id_kriteria]=' ';
                      array_push($id_kriteria,$k->id_kriteria);
                      
                    ?>
                    @endforeach


                    
                </thead>
                <tbody>
                  <?php $no=1; ?>
                    @foreach($siswa as $s)
                      <tr>
                        <td>{{ $no }}</td>
                        <td>
                        <a href="{{ url('form-nilai/'.$s->id_siswa) }}">{{ $s->nama }}</a>
                        </td>
                        
                        
                        
                        @foreach($s->nilai_normalisasi as $nilai_normalisasi)
                          @for($i=0; $i<$jumlah_kriteria; $i++)
                            @if($id_kriteria[$i]==$nilai_normalisasi->id_kriteria)
                              <?php

                                $nilai_kriteria[$id_kriteria[$i]]=$nilai_normalisasi->nilai_normalisasi;
                              ?>
                            @endif
                          @endfor
                        @endforeach                      


                        @foreach($nilai_kriteria as $nk)

                          <td>{{$nk}}</td>
                        @endforeach

                        @foreach($kriteria as $kt)
                          <?php
                            $nilai_kriteria[$kt->id_kriteria]=' ';
                          ?>

                        @endforeach



                      </tr>
                      <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
          </div>

<style>
  table,td,th{
    white-space:nowrap;
  }
  </style>


@endsection
