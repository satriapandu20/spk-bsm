@extends('layout/layout-spk')

@section('title','TOPSIS')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>TOPSIS</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> TOPSIS</li>
            </ol>

          </div>
</div><!-- /.row -->

            <!-- <a href="{{ url('proses-normalisasi') }}"><button type="button" class="btn btn-info">Proses Normalisasi</button></a>
            <a href="{{ url('proses-ternormalisasi') }}"><button type="button" class="btn btn-info">Proses ternormalisasi</button></a>
            <a href="{{ url('proses-ternormalisasi-terbobot') }}"><button type="button" class="btn btn-info">Proses ternormalisasi terbobot</button></a>
            <a href="{{ url('proses-solusi-ideal') }}"><button type="button" class="btn btn-info">Proses Solusi Ideal</button></a>
            <a href="{{ url('proses-jarak-solusi-ideal') }}"><button type="button" class="btn btn-info">Proses Jarak Solusi Ideal</button></a>
            <a href="{{ url('proses-preferensi') }}"><button type="button" class="btn btn-info">Proses Preferensi</button></a> -->
            <a href="{{ url('proses-TOPSIS') }}"><button type="button" class="btn btn-success">Proses TOPSIS</button></a>
            <a href="{{ url('reset-TOPSIS') }}"onclick="return confirm('Yakin Reset ?')"><button type="button" class="btn btn-danger">Reset</button></a>

@endsection