@extends('layout/layout-spk')

@section('title','Form Nilai')
@section('content')
<?php 



?>
<div class="row">
          <div class="col-lg-12">
            <h1>Form Input Nilai</h1>
            <ol class="breadcrumb">
            <li class="active"><i class="fa fa-edit"></i> {{ $siswa->nama }}</li>

            </ol>
           
          </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('form-nilai/'.$siswa->id_siswa) }}" method="post" >
            <!-- wajib -->
            {{ csrf_field() }}

            @foreach($kriteria as $kriteria)

            <div class="form-group">
                <label>{{ $kriteria->nama_kriteria }}</label>
                <div class="form-group" style="">
                    
                    <select name="nilai_kriteria['{{ $kriteria->id_kriteria }}']" class="form-control">
                        <option value=''>Pilih Kondisi</option>
                    @foreach($nilai_kriteria as $nilai)

                    @if($nilai->id_kriteria==$kriteria->id_kriteria){
                        <option value='{{ $nilai->nilai }}'>{{$nilai->kondisi}}</option>

              
                    @endif
                    @endforeach

                    </select>
                    
                </div>
                <!-- <select>
                @foreach($nilai_kriteria as $nilai)
                
                @if($nilai->id_kriteria==$kriteria->id_kriteria)
                <option value='$nilai->nilai'>{{$nilai->kondisi}}</option>
                @endif
                @endforeach
                </select> -->
            </div>
            @endforeach
            
            

            <button type="submit" class="btn btn-default">Simpan</button>




        </form>
    </div>
</div>
@endsection