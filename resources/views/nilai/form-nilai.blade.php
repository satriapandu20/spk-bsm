@extends('layout/layout-spk')

@section('title','Form Nilai')
@section('content')
<?php 
    $jumlah_nilai=count($nilai);
    

?>
<div class="row">
          <div class="col-lg-12">
            <h1>Form Input Nilai</h1>
            <ol class="breadcrumb">
            <li class="active"><i class="fa fa-edit"></i> {{ $siswa->nama }}</li>

            </ol>
           
          </div>
</div><!-- /.row -->




<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        @if($jumlah_nilai==0)
        <form role="form" action="{{ url('form-nilai/'.$siswa->id_siswa) }}" method="post" >
        @else
        <form role="form" action="{{ url('edit-nilai/'.$siswa->id_siswa) }}" method="post" >
        <input type="hidden" name="_method" value='PUT'>
        @endif
            <!-- wajib -->
            {{ csrf_field() }}

        <table class="table table-bordered table-hover tablesorter">
        <tr>

        @foreach($kriteria as $k)
            <th> {{$k->nama_kriteria}} </th>
        @endforeach
        
        </tr>

        <tr>
     
        @foreach($kriteria as $k)
            <td>
            <select name="nilai_kriteria['{{ $k->id_kriteria }}']">
            <option value="">Pilih Jawaban</option>
            @if($jumlah_nilai==0)
                @foreach($k->nilai_kriteria as $nk)

                <option value="{{$nk->nilai}}">{{$nk->kondisi}}</option>
                

                @endforeach
            @else
                @foreach($k->nilai_kriteria as $nk)
                
                    @foreach($nilai as $nl)
                        @if($nk->id_kriteria==$nl->id_kriteria && $nk->nilai==$nl->nilai)
                            <option value="{{$nk->nilai}}" selected>{{$nk->kondisi}}</option>
                        @elseif($nk->id_kriteria==$nl->id_kriteria)
                            <option value="{{$nk->nilai}}">{{$nk->kondisi}}</option>
                        @endif
                        
                    @endforeach    



                @endforeach

            @endif
            </select>
            </td>

        @endforeach
        </tr>

        </table>
            
    </div>
</div>
<br>


<button type="submit" class="btn btn-default">Simpan</button>




        </form>


        <style>
  table,td,th{
    white-space:nowrap;
  }
  </style>

@endsection