@extends('layout/layout-spk')

@section('title','Form Edit Data Nilai Kriteria')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Edit Data Nilai Kriteria</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-nilai-kriteria/'.$kriteria->id_kriteria) }}"><i class="fa fa-dashboard"></i> Data Nilai Kriteria</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Nilai Kriteria</li>
            </ol>
           
          </div>
</div><!-- /.row -->


<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('edit-nilai-kriteria/'.$kriteria->id_kriteria.'/'.$nilai_kriteria->id_nilai_kriteria) }}" method="post" >
            <!-- wajib -->
            <input type="hidden" name="_method" value='PUT'>
            {{ csrf_field() }}

            <div class="form-group">
                <label>Kriteria</label>

                <select name="id_kriteria" class="form-control" disabled>
          

                        @if( $nilai_kriteria->id_kriteria==$kriteria->id_kriteria)
                            <option value='{{ $kriteria->id_kriteria }}' selected>{{ $kriteria->nama_kriteria }}</option>

                        
                        @else
                            <option value='{{ $kriteria->id_kriteria }}'>{{ $kriteria->nama_kriteria }}</option>
                        @endif



        
                </select>
            </div>

            <div class="form-group">
                <label>Kondisi</label>
                <input class="form-control" type="text" name="kondisi" value='{{ $nilai_kriteria->kondisi }}'>
            </div>

            <div class="form-group">
                <label>Nilai</label>
                <input class="form-control" type="text" name="nilai" value='{{ $nilai_kriteria->nilai }}'>
            </div>


            <button type="submit" class="btn btn-default">Edit</button>



        </form>
    </div>
</div>
@endsection

