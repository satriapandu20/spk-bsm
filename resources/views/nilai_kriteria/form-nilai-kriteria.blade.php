@extends('layout/layout-spk')

@section('title','Form Nilai Kriteria')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Input Nilai kriteria</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-nilai-kriteria/'.$kriteria->id_kriteria) }}"><i class="fa fa-dashboard"></i> Data Nilai Kriteria</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Nilai Kriteria</li>
            </ol>
           
          </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('form-nilai-kriteria/'.$kriteria->id_kriteria) }}" method="post" >
            <!-- wajib -->
            {{ csrf_field() }}

            <div class="form-group">
                <label>Kriteria</label>
                <div class="form-group" >
          
                    <select name="id_kriteria" class="form-control" id="disabledSelect" disabled >
                

                        
                        <option value='{{ $kriteria->id_kriteria}}'>{{ $kriteria->nama_kriteria }}</option>

                        
                    
                    
                    </select>
                    
                </div>
            </div>

            <div class="form-group">
                <label>Kondisi</label>
                <input class="form-control" type="text" name="kondisi">
            </div>  

            <div class="form-group">
                <label>Nilai</label>
                <input class="form-control" type="text" name="nilai">
            </div>

            <button type="submit" class="btn btn-default">Tambahkan</button>

       </form>
    </div>
</div>


@endsection