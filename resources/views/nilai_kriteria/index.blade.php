@extends('layout/layout-spk')

@section('title','Data Nilai Kriteria')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Data Nilai Kriteria</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> Data Nilai Kriteria</li>
            </ol>
            <a href="{{ url('form-nilai-kriteria/'.$kriteria->id_kriteria) }}"><button type="button" class="btn btn-success">Tambah</button></a>
          </div>
</div><!-- /.row -->
<br>
<div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th class='col-lg-1'>No</th>
                    <th>Kriteria <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Kondisi <i class="fa fa-sort"></i></th>
                    <th  class='col-lg-1'>Nilai <i class="fa fa-sort"></i></th>
                    <th class='col-lg-2'> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; ?>
                    @foreach($nilai_kriteria as $nilai_kriteria)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $nilai_kriteria->nama_kriteria }}</td>
                      <td align='center'>{{ $nilai_kriteria->kondisi }}</td>
                      <td align='center'>{{ $nilai_kriteria->nilai }}</td>
                      <td align='center'>
                          <a href="{{ url('edit-nilai-kriteria/'.$kriteria->id_kriteria.'/'.$nilai_kriteria->id_nilai_kriteria) }}"><span class="label label-info">Edit</span></a>
                          <a href="{{ url('hapus-nilai-kriteria/'.$kriteria->id_kriteria.'/'.$nilai_kriteria->id_nilai_kriteria) }}" onclick="return confirm('Yakin Hapus ?')"><span class="label label-danger">Hapus</span></a>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>


@endsection

