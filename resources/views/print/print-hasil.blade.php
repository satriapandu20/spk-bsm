
<html>

<head><title>Cetak</title></head>
<style type="text/css">
body{
    width: 100%;
    margin:auto;
    padding-top:-10;

}
.header{
    text-align: center;
    letter-spacing: 0.5px;
    line-height: 1.5;
}
.atas{
    font-size:14px;
}
.bawah{
    font-size: 14px;
}
table{
    width: 100%;

}
.isian table, .isian th, .isian td{
    border:1px solid #000;
    border-collapse: collapse;
    padding: 5px;
}

.content{
    


}

td p{
    text-transform: uppercase;
    padding-top: -13px;
    padding-bottom: -13px;
}
</style>
<body>

<div class="content">

    <table>
    <tr>
    <td><img src="{!! asset('public/logosmpm3.png')!!}" width="150"></td>
    <td>
        <div class="header">
                 <p>
                <div class="atas">MUHAMMADIYAH MAJLIS PENDIDIKAN DASAR DAN MENENGAH</div>
                <b style="font-size: 23px;text-decoration:Bold;">SMP MUHAMMADIYAH 3 PURWOKERTO <br> TERAKREDITASI “ A “</b> 
                <!-- <br> <b style="font-size: 24px;text-decoration:underline;">TERAKREDITASI “ A “</b> -->
                <br>
                <div class="bawah">
                Alamat : Jl. Dr. Angka No. 79 (0281) 638773 Purwokerto 53121
                smpmuhammadiyah3pwt@gmail.com

                </div>
                </p>
        </div>
    </td>
    </tr>
    </table>
    <hr style="height: 1px; margin-top:0; background: #000;">



    <table>

    <tr><td>
    <div class="header">
    <b style="font-size: 18px;text-align: center;text-decoration: underline;">Laporan BSM Hasil SPK </b>

    </div>
    <br>
    </td></tr>

    </table>
    <div class="isian">
    <div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    
                    <th>NIK <i class="fa fa-sort"></i></th>
                    <th>Nama <i class="fa fa-sort"></i></th>
                    <th align='center'>Kelas <i class="fa fa-sort"></i></th>
                    <th align='center'>Nilai <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1' align='center'>Rangking</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; ?>
                    @foreach($siswa as $siswa)
                    <tr>
                      <td>{{ $siswa->NIK }}</td>
                      <td>{{ $siswa->nama }}</td>
                      <td align='center'>{{ $siswa->kelas }}{{ $siswa->rombel }}</td>
                      <td align='center'>{{ substr ($siswa->nilai_preferensi,0,6) }}</td>
                      <td align='center'>{{ $no }}</td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
    </div>
    </div>
    <br>

    <table style="border:1px solid #fff;">
        <tr>
        <td width="60%"></td>
        <td>
        <?php

        date_default_timezone_set('Asia/Jakarta');
            $y=date('Y');
            $d=date('d');
            $m=date('m');

            if($m=='01'){
                $m='Januari';
            }else if($m=='02'){
                $m='Februari';
            }else if($m=='03'){
                $m='Maret';
            }else if($m=='04'){
                $m='April';
            }else if($m=='05'){
                $m='Mei';
            }else if($m=='06'){
                $m='Juni';
            }else if($m=='07'){
                $m='Juli';
            }else if($m=='08'){
                $m='Agustus';
            }else if($m=='09'){
                $m='September';
            }else if($m=='10'){
                $m='Oktober';
            }else if($m=='11'){
                $m='November';
            }else if($m=='12'){
                $m='Desember';
            }

        ?>
            <p style="text-transform: none;">Purwokerto, {{$d}} {{$m}} {{$y}} <br> Menyetujui,</p>
            <br>
            <br>
            <br>
            <p>
            <b style="text-decoration: underline;text-transform: uppercase;"></b>
            <br>
          

         
        </td>
        </tr>
    </table>

</div>

</body>
</html>