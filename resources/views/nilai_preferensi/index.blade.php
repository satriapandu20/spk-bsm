@extends('layout/layout-spk')

@section('title','Data Nilai Preferensi')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Data Nilai Preferensi</h1>
            <a href="{{ url('print-hasil') }}"><button type="button" class="btn btn-success">Print Hasil</button></a>
            <br><br>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> Data Nilai Preferensi</li>
            </ol>
            </div>
            <form role="form" action="{{ url('cari-nilai-preferensi') }}" method="get" >
                <div class="form-group">
                    <input class="form-control" type="text" name="keyword" placeholder='Cari nilai siswa...'>
                </div>

                <button type="submit" class="btn btn-default">Cari</button>
            </form>
</div><!-- /.row -->
<br>
<div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th class='col-lg-1'>Rangking</th>
                    <th>Nama <i class="fa fa-sort"></i></th>
                    <th>Nilai Preferensi <i class="fa fa-sort"></i></th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; ?>
                    @foreach($siswa as $siswa)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $siswa->nama }}</td>
                      <td align='center'>{{ substr ($siswa->nilai_preferensi,0,6) }}</td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>


@endsection

