@extends('layout/layout-spk')

@section('title','Form Kriteria')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Input Kriteria</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-kriteria') }}"><i class="fa fa-dashboard"></i> Kriteria</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Kriteria</li>
            </ol>
           
          </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('form-kriteria') }}" method="post" >
            <!-- wajib -->
            {{ csrf_field() }}

            <div class="form-group">
                <label>Kriteria</label>
                <input class="form-control" type="text" name="nama_kriteria">
            </div>

            <div class="form-group">
                <label>Bobot</label>
                <div class="form-group" style="margin-left:-13px;">
                    <div class="col-lg-4">
                    <select name="bobot" class="form-control">
                    <option value=''>Pilih Bobot</option>
                    <option value='0.2'>0.2</option>
                    <option value='0.1'>0.1</option>
                    <option value='0.05'>0.05</option>
                    <option value='0.025'>0.025</option>
                    </select>
                    </div>
                </div>
            </div>
            <br>
            <br>

            <div class="form-group">
                <label>Sifat</label>
                <br>
                <label class="radio-inline">
                  <input type="radio" name="sifat" id="optionsRadiosInline1" value="cost"> Cost
                </label>
                <label class="radio-inline">
                  <input type="radio" name="sifat" id="optionsRadiosInline2" value="benefit"> Benefit
                </label>
            </div>

            <div class="form-group">
                <label>Keterangan</label>
                <input class="form-control" type="text" name="keterangan">
            </div>

            <button type="submit" class="btn btn-default">Tambahkan</button>




        </form>
    </div>
</div>
@endsection

