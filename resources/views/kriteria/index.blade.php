@extends('layout/layout-spk')

@section('title','Data Kriteria')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Data Kriteria</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> Data Kriteria</li>
            </ol>
            <a href="{{ url('form-kriteria') }}"><button type="button" class="btn btn-success">Tambah</button></a>
          </div>
</div><!-- /.row -->
<br>
<div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th class='col-lg-1'>No</th>
                    <th>Kriteria <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Bobot <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Sifat <i class="fa fa-sort"></i></th>
                    <th>Keterangan <i class="fa fa-sort"></i></th>
                    <th class='col-lg-2'> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; ?>
                    @foreach($kriteria as $kriteria)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $kriteria->nama_kriteria }}</td>
                      <td align='center'>{{ $kriteria->bobot }}</td>
                      <td align='center'>{{ $kriteria->sifat }}</td>
                      <td>{{ $kriteria->keterangan }}</td>
                      <td align='center'>
                          <a href="{{ url('data-nilai-kriteria/'.$kriteria->id_kriteria) }}"><span class="label label-warning">Nilai</span></a>
                          <a href="{{ url('edit-kriteria/'.$kriteria->id_kriteria) }}"><span class="label label-info">Edit</span></a>
                          <a href="{{ url('hapus-kriteria/'.$kriteria->id_kriteria) }}" onclick="return confirm('Yakin Hapus ?')"><span class="label label-danger">Hapus</span></a>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>


@endsection
