@extends('layout/layout-spk')

@section('title','Form Edit Data Kriteria')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Edit Data Kriteria</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-kriteria') }}"><i class="fa fa-dashboard"></i> Data Kriteria</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Kriteria</li>
            </ol>
           
          </div>
</div><!-- /.row -->


<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('edit-kriteria/'.$kriteria->id_kriteria) }}" method="post" >
            <!-- wajib -->
            <input type="hidden" name="_method" value='PUT'>
            {{ csrf_field() }}

            <div class="form-group">
                <label>Kriteria</label>
                <input class="form-control" type="text" name="nama_kriteria" value='{{ $kriteria->nama_kriteria }}'>
            </div>

            <div class="form-group">
                <label>Bobot </label>
                <div class="form-group" style="margin-left:-13px;">
                    <div class="col-lg-4">
                <?php 
                    $ke1='';
                    $ke2='';
                    $ke3='';
                    $ke4='';
                    if($kriteria->bobot=='0.2')
                    {
                        $ke1='selected';
                    }
                    else if($kriteria->bobot=='0.1')
                    {
                        $ke2='selected';
                    }
                    else if($kriteria->bobot=='0.05')
                    {
                        $ke3='selected';
                    }
                    else if($kriteria->bobot=='0.025')
                    {
                        $ke4='selected';
                    }
                ?>
                    <select name="bobot" class="form-control">
                        <option value='0.2' {{ $ke1 }}>0,2 </option>
                        <option value='0.1' {{ $ke2 }}>0,1</option>
                        <option value='0.05' {{ $ke3 }}>0,05 </option>
                        <option value='0.025' {{ $ke4 }}>0,025 </option>
                    </select>
                    </div>
                </div>
            </div>
            <br>
            <br>

            <div class="form-group">
                <label>Sifat</label>
                <br>
                <?php 
                    $cost='';
                    $benefit='';
                    if($kriteria->sifat=='cost')
                    {
                        $cost='checked';
                    }
                    else if($kriteria->sifat=='benefit')
                    {
                        $benefit='checked';
                    }
                ?>
                <label class="radio-inline">
                  <input type="radio" name="sifat" id="optionsRadiosInline1" value="cost" {{ $cost }}> Cost
                </label>
                <label class="radio-inline">
                  <input type="radio" name="sifat" id="optionsRadiosInline2" value="benefit" {{ $benefit }}> Benefit
                </label>
            </div>

            <div class="form-group">
                <label>Keterangan</label>
                <input class="form-control" type="text" name="keterangan" value='{{ $kriteria->keterangan }}'>
            </div>

            <button type="submit" class="btn btn-default">Edit</button>



        </form>
    </div>
</div>
@endsection

