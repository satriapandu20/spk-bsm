@extends('layout/layout-spk')

@section('title','Form Mahasiswa')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Input Nama Mahasiswa</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-mahasiswa') }}"><i class="fa fa-dashboard"></i> Data Nama Mahasiswa</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Buku</li>
            </ol>
           
          </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('form-mahasiswa') }}" method="post" >
            <!-- wajib -->
            {{ csrf_field() }}

            <div class="form-group">
                <label>Nama Depan</label>
                <input class="form-control" type="text" name="nama_depan">
            </div>

            <div class="form-group">
                <label>Nilai</label>
                <input class="form-control" type="text" name="nilai">
            </div>









            <button type="submit" class="btn btn-default">Coba</button>


            
        </form>
    </div>
</div>
@endsection