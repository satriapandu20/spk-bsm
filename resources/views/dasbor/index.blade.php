@extends('layout/layout-spk')

@section('title','Sistem Pendukung Keputusan SMP Muhammadiyah 3 Purwokerto')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h2>Sistem Pendukung Keputusan SMP Muhammadiyah 3 Purwokerto</h2>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i>Penentuan Penerima Bantuan Siswa Miskin (BSM)</li>
            </ol>

          </div>
</div><!-- /.row -->

<div class="row">
          <div class="col-lg-3">
            <div class="panel panel-info">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-6">
                  <i class="fa fa-tasks fa-5x"></i>

                  </div>
                  <div class="col-xs-6 text-right">
                    <p class="announcement-heading">{{$siswa}}</p>
                    <p class="announcement-text">Siswa</p>
                  </div>
                </div>
              </div>
              <a href="{{url('data-siswa')}}">
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      Lihat Data
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>



@endsection