@extends('layout/layout-spk')

@section('title','Form Buku')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Input Buku</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-buku') }}"><i class="fa fa-dashboard"></i> Data Buku</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Buku</li>
            </ol>
           
          </div>
</div><!-- /.row -->


<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('form-buku') }}" method="post" >
            <!-- wajib -->
            {{ csrf_field() }}

            <div class="form-group">
                <label>Judul Buku</label>
                <input class="form-control" type="text" name="judul_buku">
            </div>

            <div class="form-group">
                <label>Penerbit</label>
                <div class="form-group" style="margin-left:-13px;">
                    <div class="col-lg-4">
                    <select name="penerbit" class="form-control">
                        <option value='erlangga'>Erlangga</option>
                        <option value='gramedia'>Gramedia</option>
                        <option value='amikom'>Amikom</option>
                    </select>
                    </div>
                </div>
            </div>
            <br>
            <br>

            <div class="form-group">
                <label >Tahun Terbit</label>
                <input class="form-control" type="text" name="tahun_terbit">
            </div>

            <div class="form-group">
                <label>Tanggal Terbit</label>
                <input class="form-control" type="date" name="tanggal_terbit">
            </div>

            <div class="form-group">
                <label>Ketersediaan</label>
                <br>
                <label class="radio-inline">
                  <input type="radio" name="ketersediaan" id="optionsRadiosInline1" value="ada"> Ada
                </label>
                <label class="radio-inline">
                  <input type="radio" name="ketersediaan" id="optionsRadiosInline2" value="tidak"> Tidak
                </label>
            </div>

            <div class="form-group">
                <label>Keterangan</label>
                <input class="form-control" type="text" name="keterangan">
            </div>
            
            <button type="submit" class="btn btn-default">Tambahkan</button>


            
        </form>
    </div>
</div>
@endsection

