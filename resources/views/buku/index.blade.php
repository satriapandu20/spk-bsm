@extends('layout/layout-spk')

@section('title','Data Buku')
@section('content')

<?php
// $txt = "Hello Welcome to SPK BSM!";
// $x = 5;
// $y = 10.5;

// echo $txt;
// echo "<br>";
// echo $x;
// echo "<br>";
// echo $y;

// $txt = "W3Schools.com";

// echo "I love $txt!";
// echo "I love " . $txt . "!";

// $x = 5;
// $y = 4;

// echo $x + $y;

// $x = 5;
// $y = 10;

// function myTest() {
//     global $x, $y;
//     $y = $x + $y;
// }

// myTest();
// echo $y; // outputs 15
// echo "<br>";
// echo $y - $x; //5
// echo "<br>";
// echo $y + $x; //15
// echo "<br>";
// echo $y / $x;
// echo "<br>";
// echo ($x / $x) - ($y + $y) ;
// echo "<br>";
// echo $y * $x;

//jari jari sebesar 10cm berapa luas lingkaran ?
// $r = 10*10;
// $p = 3.14;

// echo $p * $r;

//keliling lingkaran dengan jari-jari 20cm
// $r = 20;
// $p = 3.14;

// echo 2 * $p * $r;

//keliling lingkaran dengan diameter 20cm
$phi=22/7;
$d = 20;

$keliling = $d * $phi;

$luas=$keliling*2;


echo $luas;

//


// $x = 5; // global scope

// function myTest() {
//     // using x inside this function will generate an error
//     echo "<p>Variable x inside function is: $x</p>";
// } 
// myTest();

// echo "<p>Variable x outside function is: $x</p>";

?>
<div class="row">
          <div class="col-lg-12">
            <h1>Data Buku</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> Data Buku</li>
            </ol>
            <a href="{{ url('form-buku') }}"><button type="button" class="btn btn-success">Tambah</button></a>
          </div>
</div><!-- /.row -->
<br>
<div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th class='col-lg-1'>No</th>
                    <th>Judul Buku <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Penerbit <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Tahun Terbit <i class="fa fa-sort"></i></th>
                    <th>Tanggal Terbit <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Ketersediaan <i class="fa fa-sort"></i></th>
                    <th>Keterangan <i class="fa fa-sort"></i></th>
                    <th class='col-lg-2'> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; ?>
                    @foreach($buku as $buku)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $buku->judul_buku }}</td>
                      <td align='center'>{{ $buku->penerbit }}</td>
                      <td align='center'>{{ $buku->tahun_terbit }}</td>
                      <td>{{ $buku->tanggal_terbit }}</td>
                      <td align='center'>{{ $buku->ketersediaan }}</td>
                      <td>{{ $buku->keterangan }}</td>                      
                      <td align='center'>
                          <a href="{{ url('edit-buku/'.$buku->id_buku) }}"><span class="label label-info">Edit</span></a>
                          <a href="{{ url('hapus-buku/'.$buku->id_buku) }}" onclick="return confirm('Yakin Hapus ?')"><span class="label label-danger">Hapus</span></a>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>


@endsection


