@extends('layout/layout-spk')

@section('title','Data Nilai Solusi Ideal Positif Negatif')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Data Nilai Solusi Ideal Positif Negatif</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> Data Nilai Solusi Ideal Positif Negatif</li>
            </ol>
          </div>
          <form role="form" action="{{ url('cari-nilai-solusi-ideal') }}" method="get" >
                <div class="form-group">
                    <input class="form-control" type="text" name="keyword" placeholder='Cari nilai siswa...'>
                </div>

                <button type="submit" class="btn btn-default">Cari</button>
          </form>
</div><!-- /.row -->
<br>
<div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th class='col-lg-1'>No</th>
                    <th>Kriteria <i class="fa fa-sort"></i></th>
                  
                    <th class='col-lg-1'>Sifat <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>A+ <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>A- <i class="fa fa-sort"></i></th>

                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; ?>
                  @foreach($kriteria as $kriteria)
                    <tr>

                      <td>{{ $no }}</td>
                      <td>{{ $kriteria->nama_kriteria }}</td>

                      <td align='center'>{{ $kriteria->sifat }}</td>
                      <td align='center'>{{ substr ($kriteria->a_plus,0,6) }}</td>
                      <td align='center'>{{ substr ($kriteria->a_minus,0,6) }}</td>

                    </tr>
                    <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>

<style>
  table,td,th{
    white-space:nowrap;
  }
  </style>


@endsection
