
<!DOCTYPE html>
<html>

<head>
	<title>@yield('title')</title>



  	<!-- <link rel="stylesheet" href="{{ url('public/css/bootstrap.min.css') }}" type="text/css" />
  	<link rel="stylesheet" href="{{ url('public/css/bootstrap.css') }}" type="text/css" /> -->


	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
	   <!-- Bootstrap Core CSS -->

    <!-- <link href="{!! asset('theme/vendor/bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet"> -->

    


    <!-- MetisMenu CSS -->

    <!-- <link href="{!! asset('theme/vendor/metisMenu/metisMenu.min.css') !!}" rel="stylesheet"> -->



    <!-- Custom CSS -->

    <!-- <link href="{!! asset('theme/dist/css/sb-admin-2.css') !!}" rel="stylesheet"> -->



    <!-- Morris Charts CSS -->

    <!-- <link href="{!! asset('theme/vendor/morrisjs/morris.css') !!}" rel="stylesheet"> -->



    <!-- Custom Fonts -->

    <!-- <link href="{!! asset('theme/vendor/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css"> -->


    <link href="{!! asset('public/css/more.css') !!}" rel="stylesheet">

    <!-- Jquery -->

    <link href="{!! asset('theme/vendor/jquery/jquery.min.js') !!}" rel="stylesheet">

</head>

<body>
<div id="cetak">

	
    @include('layout.flash-message')
	@yield('content')
	
	
</div>
</body>
</html>