<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{!! asset('tema/css/bootstrap.css') !!}" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="{!! asset('tema/css/sb-admin.css') !!}" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('tema/font-awesome/css/font-awesome.min.css') !!}">
    <!-- Page Specific CSS -->
    <!-- <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css"> -->

    <style>
      .testimonial-group > .scroll{
        overflow-x: auto;
        white-space: nowrap;
      }
      .testimonial-group > .scroll > .tesx {
        display: inline-block;
        float: none;

      }
    </style>
  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ url('') }}">SMP Muhammadiyah 3 Purwokerto</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
            
            @if(Session::get('superadmin'))
            <li><a href="{{ url('data-nilai-preferensi') }}"><i class="fa fa-table"></i> Hasil Print Laporan</a></li>
            @endif

            @if(Session::get('admin'))
            <li ><a href="{{ url('/') }}"><i class="fa fa-home"></i> Dashboard</a></li>
            <li><a href="{{ url('data-admin') }}"><i class="fa fa-user"></i> Admin</a></li>
            <li><a href="{{ url('data-siswa') }}"><i class="fa fa-table"></i> Data Siswa</a></li>
            <li><a href="{{ url('data-kriteria') }}"><i class="fa fa-table"></i> Kriteria</a></li>
            <li><a href="{{ url('data-nilai') }}"><i class="fa fa-table"></i> Data Nilai</a></li>
            <!-- <li><a href="{{ url('data-nilai-kriteria') }}"><i class="fa fa-table"></i> Data Nilai Kriteria</a></li> -->
            <!-- <li><a href="{{ url('data-buku') }}"><i class="fa fa-table"></i> Data Buku</a></li> -->
            <li class="dropdown">
              <a href="{{ url('data-siswa') }}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> TOPSIS <b class="caret"></b></a>
              <ul class="dropdown-menu">
                  <li><a href="{{ url('TOPSIS') }}"><i class="fa fa-table"></i> Proses TOPSIS</a></li>
                  <li><a href="{{ url('data-nilai-normalisasi') }}"><i class="fa fa-table"></i> Normalisasi</a></li>
                  <li><a href="{{ url('data-nilai-ternormalisasi') }}"><i class="fa fa-table"></i> Ternormalisasi</a></li>
                  <li><a href="{{ url('data-nilai-ternormalisasi-terbobot') }}"><i class="fa fa-table"></i> Ternormalisasi Terbobot</a></li>
                  <li><a href="{{ url('data-nilai-solusi-ideal') }}"><i class="fa fa-table"></i> Solusi Ideal Positif Negatif</a></li>
                  <li><a href="{{ url('data-nilai-jarak-solusi-ideal') }}"><i class="fa fa-table"></i> Jarak Solusi Ideal</a></li>
                  <li><a href="{{ url('data-nilai-preferensi') }}"><i class="fa fa-table"></i> Preferensi</a></li>
              </ul>
            </li>
            @endif
              
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
            
            <li class="dropdown user-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
              @if(Session::get('superadmin'))
               Super Admin 
              @else
               Admin 
              @endif
               <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <!-- <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
                <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li> -->
                <li class="divider"></li>
                <li><a class="dropdown-item" href="{{ url('proseslogout') }}">
                                       Logout
                                    </a>

              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">
      @include('layout/flash-message')
      @yield('content')

      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->


    <!-- JavaScript -->
    <script src="{!! asset('tema/js/jquery-1.10.2.js') !!}"></script>
    <script src="{!! asset('tema/js/bootstrap.js') !!}"></script>

    <!-- Page Specific Plugins -->
    <!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script> -->
    <script src="{!! asset('tema/js/morris/chart-data-morris.js') !!}"></script>
    <script src="{!! asset('tema/js/tablesorter/jquery.tablesorter.js') !!}"></script>
    <script src="{!! asset('tema/js/tablesorter/tables.js') !!}"></script>
    <script type="text/javascript">

    function hanyaAngka(evt) {
      var charCode = (evt.which) ? evt.which : event.keyCode
      if (charCode > 31 && (charCode < 48 || charCode > 57))

      return false;
      return true;

    }
    </script>
  </body>
</html>
