@extends('layout/layout-spk')

@section('title','Nilai Jarak Solusi Ideal')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Data Nilai Jarak Solusi Ideal</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> Data Nilai Jarak Solusi Ideal</li>
            </ol>
            </div>
            <form role="form" action="{{ url('cari-nilai-jarak-solusi-ideal') }}" method="get" >
                <div class="form-group">
                    <input class="form-control" type="text" name="keyword" placeholder='Cari nilai siswa...'>
                </div>

                <button type="submit" class="btn btn-default">Cari</button>
            </form>

</div><!-- /.row -->
<br>
<div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th class='col-lg-1'>No</th>
                    <th>Nama <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>D+ <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>D- <i class="fa fa-sort"></i></th>

                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; ?>
                    @foreach($siswa as $siswa)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $siswa->nama }}</td>
                      <td align='center'>{{ substr ($siswa->d_plus,0,6) }}</td>
                      <td align='center'>{{ substr ($siswa->d_minus,0,6) }}</td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>


@endsection

