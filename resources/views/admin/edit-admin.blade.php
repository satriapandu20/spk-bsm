@extends('layout/layout-spk')

@section('title','Form Edit Data Admin')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Edit Data Admin</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-admin') }}"><i class="fa fa-dashboard"></i> Data Admin</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Admin</li>
            </ol>
           
          </div>
</div><!-- /.row -->


<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('edit-admin/'.$admin->id_admin) }}" method="post" >
            <!-- wajib -->
            <input type="hidden" name="_method" value='PUT'>
            {{ csrf_field() }}

            <div class="form-group">
                <label>Username</label>
                <input class="form-control" type="text" name="username" value='{{ $admin->username }}'>
            </div>

            <div class="form-group">
                <label>Password</label>
                <input class="form-control" type="password" name="password">
            </div>

            <div class="form-group">
                <label>Konfirmasi Password</label>
                <input class="form-control" type="password" name="password_confirmation">
            </div>

            <button type="submit" class="btn btn-default">Edit</button>



        </form>
    </div>
</div>
@endsection

