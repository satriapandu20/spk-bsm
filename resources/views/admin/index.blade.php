@extends('layout/layout-spk')

@section('title','Data Admin')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Data Admin</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> Data Admin</li>
            </ol>
            <a href="{{ url('form-admin') }}"><button type="button" class="btn btn-success">Tambah</button></a>
          </div>
</div><!-- /.row -->
<br>
<div class="row">
          <div class="col-lg-8">
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th class='col-lg-1'>No</th>
                    <th class='col-lg-1'>Username <i class="fa fa-sort"></i></th>
                    <!-- <th class='col-lg-1'>Passwod <i class="fa fa-sort"></i></th> -->
                    <th class='col-lg-2'> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; ?>
                    @foreach($admin as $admin)
                    <tr>
                      <td>{{ $no }}</td>
                      <td align='center'>{{ $admin->username }}</td>
                      <!-- <td align='center'>{{ $admin->password }}</td> -->
                      <td align='center'>
                          <a href="{{ url('edit-admin/'.$admin->id_admin) }}"><span class="label label-info">Edit</span></a>
                          <a href="{{ url('hapus-admin/'.$admin->id_admin) }}" onclick="return confirm('Yakin Hapus ?')"><span class="label label-danger">Hapus</span></a>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>


@endsection
