@extends('layout/layout-spk')

@section('title','Form Admin')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Admin</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-admin') }}"><i class="fa fa-dashboard"></i> Admin</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Admin</li>
            </ol>
           
          </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('form-admin') }}" method="post" >
            <!-- wajib -->
            {{ csrf_field() }}

            <div class="form-group">
                <label>Username</label>
                <input class="form-control" type="text" name="username">
            </div>

            <div class="form-group">
                <label>Password</label>
                <input class="form-control" type="password" name="password">
            </div>

            <div class="form-group">
                <label>Konfirmasi Password</label>
                <input class="form-control" type="password" name="password_confirmation">
            </div>

            <button type="submit" class="btn btn-default">Tambahkan</button>




        </form>
    </div>
</div>
@endsection

