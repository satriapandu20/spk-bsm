@extends('layout/layout-spk')

@section('title','Data Siswa')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Data Siswa</h1>
            <ol class="breadcrumb">
              <li class="active"><i class="fa fa-table"></i> Data Siswa</li>
            </ol>
            <a href="{{ url('form-siswa') }}"><button type="button" class="btn btn-success">Tambah</button></a>
          </div>
</div><!-- /.row -->
<br>
<div class="row">
          <div class="col-lg-12">
            <div class="table-responsive">
              <table class="table table-bordered table-hover tablesorter">
                <thead>
                  <tr>
                    <th class='col-lg-1'>No</th>
                    <th>NIK <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Kelas <i class="fa fa-sort"></i></th>
                    <th  class='col-lg-1'>Tahun Ajaran <i class="fa fa-sort"></i></th>
                    <th>Nama <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Jenis Kelamin <i class="fa fa-sort"></i></th>
                    <th class='col-lg-1'>Tanggal Lahir <i class="fa fa-sort"></i></th>
                    <th class='col-lg-2'> Aksi </th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; ?>
                    @foreach($siswa as $siswa)
                    <tr>
                      <td>{{ $no }}</td>
                      <td>{{ $siswa->NIK }}</td>
                      <td align='center'>{{ $siswa->kelas }}{{ $siswa->rombel }}</td>
                      <td align='center'>{{ $siswa->tahun_ajaran }}</td>
                      <td>{{ $siswa->nama }}</td>
                      <td align='center'>{{ $siswa->JK }}</td>
                      <td>{{ $siswa->tanggal_lahir }}</td>
                      <td align='center'>
                          <a href="{{ url('edit-siswa/'.$siswa->id_siswa) }}"><span class="label label-info">Edit</span></a>
                          <a href="{{ url('hapus-siswa/'.$siswa->id_siswa) }}" onclick="return confirm('Yakin Hapus ?')"><span class="label label-danger">Hapus</span></a>
                      </td>
                    </tr>
                    <?php $no++; ?>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>


@endsection

