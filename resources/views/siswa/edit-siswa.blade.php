@extends('layout/layout-spk')

@section('title','Form Edit Data Siswa')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Edit Data Siswa</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-siswa') }}"><i class="fa fa-dashboard"></i> Data Siswa</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Siswa</li>
            </ol>
           
          </div>
</div><!-- /.row -->


<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('edit-siswa/'.$siswa->id_siswa) }}" method="post" >
            <!-- wajib -->
            <input type="hidden" name="_method" value='PUT'>
            {{ csrf_field() }}



            <div class="form-group">
                <label>NIK</label>
                <input class="form-control" type="text" name="NIK" value='{{ $siswa->NIK }}' onkeypress="return hanyaAngka(event)" maxlength='16'>

            </div>        

            <div class="form-group">
                <label>Kelas & Rombel</label>
                <div class="form-group" style="margin-left:-13px;">
                    <div class="col-lg-4">
                <?php 
                    $kelas7='';
                    $kelas8='';
                    $kelas9='';
                    if($siswa->kelas=='7')
                    {
                        $kelas7='selected';
                    }
                    else if($siswa->kelas=='8')
                    {
                        $kelas8='selected';
                    }
                    else if($siswa->kelas=='9')
                    {
                        $kelas9='selected';
                    }
                ?>
                    <select name="kelas" class="form-control">
                        <option value='7' {{ $kelas7 }}>Kelas 7</option>
                        <option value='8' {{ $kelas8 }}>Kelas 8</option>
                        <option value='9' {{ $kelas9 }}>Kelas 9</option>
                    </select>
                    </div>
                    <div class="col-lg-4">
                <?php 
                    $A='';
                    $B='';
                    $C='';
                    $D='';
                    if($siswa->rombel=='A')
                    {
                        $A='selected';
                    }
                    else if($siswa->rombel=='B')
                    {
                        $B='selected';
                    }
                    else if($siswa->rombel=='C')
                    {
                        $C='selected';
                    }
                    else if($siswa->rombel=='D')
                    {
                        $D='selected';
                    }
                ?>
                    <select name="rombel" class="form-control">
                        <option value='A' {{ $A }}> A</option>
                        <option value='B' {{ $B }}> B</option>
                        <option value='C' {{ $C }}> C</option>
                        <option value='D' {{ $D }}> D</option>
                    </select>
                    </div>
                </div>
            </div>
            <br>
            <br>


            
            <div class="form-group">
                <label >Tahun Ajaran</label>
                <div class="form-group">
                <select name="tahun_ajaran" class="form-control">
                <?php
                    $tahun_pertama=2000;
                    $tahun_kedua=$tahun_pertama+1;
                    $tahun_akhir=2050;

                    while ($tahun_pertama<=$tahun_akhir)
                    {
                        $tahun_ajaran_sekarang=$tahun_pertama.'/'.$tahun_kedua;


                ?>
                        @if($tahun_ajaran_sekarang==$siswa->tahun_ajaran)
                        <option value="{{ $tahun_pertama.'/'.$tahun_kedua }}" selected> {{ $tahun_pertama.'/'.$tahun_kedua }}</option>
                        @else
                        <option value="{{ $tahun_pertama.'/'.$tahun_kedua }}"> {{ $tahun_pertama.'/'.$tahun_kedua }}</option>
                        @endif

                        
                <?php
                        $tahun_pertama++;
                        $tahun_kedua++;

                    }
                ?>           
                </select>
                </div>
            </div>






            <div class="form-group">
                <label>Nama</label>
                <input class="form-control" type="text" name="nama" value='{{ $siswa->nama }}'>
            </div>

            <div class="form-group">
                <label>Jenis Kelamin</label>
                <br>
                <?php 
                    $laki='';
                    $perempuan='';
                    if($siswa->JK=='L')
                    {
                        $laki='checked';
                    }
                    else if($siswa->JK=='P')
                    {
                        $perempuan='checked';
                    }
                ?>
                <label class="radio-inline">
                  <input type="radio" name="JK" id="optionsRadiosInline1" value="L" {{ $laki }}> Laki-laki
                </label>
                <label class="radio-inline">
                  <input type="radio" name="JK" id="optionsRadiosInline2" value="P" {{ $perempuan }}>  Perempuan
                </label>
            </div>

            <div class="form-group">
                <label>Tempat Lahir</label>
                <input class="form-control" type="text" name="tempat_lahir" value='{{ $siswa->tempat_lahir }}'>
            </div>

            <div class="form-group">
                <label>Tanggal Lahir</label>
                <input class="form-control" type="date" name="tanggal_lahir" value='{{ $siswa->tanggal_lahir }}'>
            </div>

            <div class="form-group">
                <label>Alamat</label>
                <textarea class="form-control" rows="4" name="alamat" >{{ $siswa->alamat }}</textarea>
            </div>

            <button type="submit" class="btn btn-default">Edit</button>



        </form>
    </div>
</div>

<style>
  table,td,th{
    white-space:nowrap;
  }
  </style>
@endsection



