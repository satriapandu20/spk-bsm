@extends('layout/layout-spk')

@section('title','Form Siswa')
@section('content')
<div class="row">
          <div class="col-lg-12">
            <h1>Form Input Siswa</h1>
            <ol class="breadcrumb">
              <li><a href="{{ url('data-siswa') }}"><i class="fa fa-dashboard"></i> Data Siswa</a></li>
              <li class="active"><i class="fa fa-edit"></i> Form Input Siswa</li>
            </ol>
           
          </div>
</div><!-- /.row -->


<div class="row">
    <div class="col-lg-6">
        <!-- wajib -->
        <!-- action dan method wajib -->
        <form role="form" action="{{ url('form-siswa') }}" method="post" >
            <!-- wajib -->
            {{ csrf_field() }}



            <div class="form-group">
                <label>NIK</label>
                <input class="form-control" type="text" name="NIK" onkeypress="return hanyaAngka(event)" maxlength='16'>
            </div>        

            <div class="form-group">
                <label>Kelas & Rombel</label>
                <div class="form-group" style="margin-left:-13px;">
                    <div class="col-lg-4">
                    <select name="kelas" class="form-control">
                        <option value=''>Pilih Kelas</option>
                        <option value='7'>Kelas 7</option>
                        <option value='8'>Kelas 8</option>
                        <option value='9'>Kelas 9</option>
                    </select>
                    </div>
                    <div class="col-lg-4">
                    <select name="rombel" class="form-control">
                        <option value=''>Pilih Rombel</option>
                        <option value='A'> A</option>
                        <option value='B'> B</option>
                        <option value='C'> C</option>
                        <option value='D'> D</option>
                    </select>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="form-group">
                <label >Tahun Ajaran</label>
                <div class="form-group" >
                    
                    <select name="tahun_ajaran" class="form-control">
                    <option value=''>Pilih Tahun Ajaran </option>
                    <?php
                    
                        $tahun_pertama=2000;
                        $tahun_kedua=$tahun_pertama+1;
                        $tahun_akhir=2050;

                        while ($tahun_pertama<=$tahun_akhir)
                        {

                    
                    ?>
                            <option value="{{ $tahun_pertama.'/'.$tahun_kedua }}"> {{ $tahun_pertama.'/'.$tahun_kedua }}</option>
                    <?php
                            $tahun_pertama++;
                            $tahun_kedua++;
                        }
                    ?>
                    </select>
                
                </div>
            </div>

            <div class="form-group">
                <label>Nama</label>
                <input class="form-control" type="text" name="nama">
            </div>

            <div class="form-group">
                <label>Jenis Kelamin</label>
                <br>
                <label class="radio-inline">
                  <input type="radio" name="JK" id="optionsRadiosInline1" value="L"> Laki-laki
                </label>
                <label class="radio-inline">
                  <input type="radio" name="JK" id="optionsRadiosInline2" value="P"> Perempuan
                </label>
            </div>

            <div class="form-group">
                <label>Tempat Lahir</label>
                <input class="form-control" type="text" name="tempat_lahir">
            </div>

            <div class="form-group">
                <label>Tanggal Lahir</label>
                <input class="form-control" type="date" name="tanggal_lahir">
            </div>

            <div class="form-group">
                <label>Alamat</label>
                <textarea class="form-control" rows="4" name="alamat" ></textarea>
            </div>

            <button type="submit" class="btn btn-default">Tambahkan</button>



        </form>
    </div>
</div>
@endsection

